module.exports = [
  {
    name: 'calendars',
    path: '/calendars',
    component: require('./components/calendly/calendars').default,
    meta: {
      title: 'Calendars',
    }
  },
  {
    name: 'calendar.show',
    path: '/calendars/:calendarID',
    component: require('./components/calendly/calendar').default,
    meta: {
      title: 'Calendar',
    }
  },
  {
    name: 'share',
    path: '/share',
    component: require('./components/share-presentation').default,
    meta: {
      title: 'Share this presentation',
    }
  },
  {
    name: 'call_report',
    path: '/call-report',
    component: require('./components/call-report/call-report.vue').default,
    meta: {
      title: 'Call Report',
    }
  },
  {
    name: 'quote',
    path: '/quote',
    component: require('./components/quote/quote.vue').default,
    meta: {
      title: 'Request for Quote',
    }
  },
  {
    name: 'tradeshow',
    path: '/tradeshow',
    component: require('./components/tradeshow/tradeshow.vue').default,
    meta: {
      title: 'Trade Show Signup',
    }
  },
  {
    name: 'leads',
    path: '/leads',
    component: require('./components/tradeshow/leads.vue').default,
    meta: {
      title: 'Trade Show Leads',
    }
  },
  {
    name: 'lead',
    path: '/leads/:leadUUID',
    component: require('./components/tradeshow/lead.vue').default,
    children: [
      {
        name: 'lead.edit',
        path: 'edit',
        component: require('./components/tradeshow/edit-lead.vue'),
        meta: {
          title: 'Edit Lead',
        }
      },
    ]
  },
  {
    name: 'settings',
    path: '/settings',
    component: require('./components/tradeshow/settings.vue').default,
    meta: {
      title: 'Settings',
    }
  },
  {
    name: 'refresh',
    path: '/refresh',
    component: require('./components/redirect.vue').default
  },
  {
    path: '*',
    redirect: '/call-report',
  },
];
