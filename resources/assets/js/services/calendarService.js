const calendars = [
  {
    id: 1,
    name: 'Chad Swain',
    url: 'https://calendly.com/chadswain',
  },
  {
    id: 2,
    name: 'Mark Stralka',
    url: 'https://calendly.com/markstralka/trade-show-appointment',
  },
  {

    id: 3,
    name: 'Rachel Youdale',
    url: 'https://calendly.com/rachelyoudale',
  },
];

export default {
  getCalendars() {
    return calendars;
  }
}
