import _ from 'lodash';
import Locker from 'lockerjs';
import {v4 as uuid} from 'uuid';
import moment from 'moment';
import mobilelocker from '../mobilelocker-tracking';

const locker = new Locker(window.localStorage);
const KEY_LEADS = 'leads';
const KEY_SOURCE = 'lead_source';

export default {
  initLeadForm() {
    return {
      //recipients: ['sales@winterequipment.com'],
      name: '',
      title: '',
      email: '',
      phone: '',
      company: '',
      street: '',
      street2: '',
      city: '',
      state: '',
      zip: '',
      country: 'USA',
      notes: '',
      url: '',
      vcard: '',
      source: this.getSource(),
      // Signature
      signature: '',
      signature_image: '',
      signature_data: '',
    };
  },
  clearLeads() {
    locker.clearSpecified([KEY_LEADS]);
  },
  resetSource() {
    locker.clearSpecified([KEY_SOURCE]);
  },
  getSource(defaultSource = 'Mobile Locker') {
    if (locker.keyExists(KEY_SOURCE)) {
      return locker.get(KEY_SOURCE);
    }
    return defaultSource;
  },
  setSource(source) {
    locker.add(KEY_SOURCE, source);
  },
  getLeads(includeDeleted = false) {
    return new Promise((resolve, reject) => {
      if (locker.keyExists(KEY_LEADS)) {
        if (includeDeleted) {
          const leads = JSON.parse(locker.get(KEY_LEADS));
          //store.commit(MUTATIONS.SET_LEADS, leads);
          return resolve(leads);
        } else {
          const leads = _.filter(JSON.parse(locker.get(KEY_LEADS)), l => {
            return _.isNull(l.deleted_at) || _.isUndefined(l.deleted_at);
          });
          //store.commit(MUTATIONS.SET_LEADS, leads);
          return resolve(leads);
        }
      }
      return resolve([]);
    })
  },
  getLead(uuid) {
    return this.getLeads(true)
      .then(leads => {
        return _.find(leads, lead => lead.uuid === uuid);
      })
  },
  uploadLead(lead) {
    //const l = _.cloneDeep(lead);
    return mobilelocker.submitForm('lead_form', lead)
      .then(result => {
        lead.uploaded_at = moment().utc().toISOString();
        return this._saveLead(lead);
      })
  },
  updateLead(lead) {
    /*return this.getLeads(true)
      .then(leads => {
        const index = _.findIndex(leads, l => l.uuid === lead.uuid);
        if (index > -1) {
          lead.updated_at = moment().utc().toISOString();
          leads[index] = lead;
          return this.saveLeads(leads)
            .then(() => {
              return lead;
            })
        } else {
          return false;
        }
      })*/
    return this._saveLead(lead);
  },
  createLead(lead) {
    return this._saveLead(lead)
  },
  _saveLead(lead) {
    const now = moment.utc();
    if (!lead.uuid) {
      lead.uuid = uuid();
      lead.created_at = now.toISOString();
      lead.deleted_at = null;
    }
    lead.updated_at = now.toISOString();
    return this.getLeads()
      .then(leads => {
        const index = _.findIndex(leads, l => l.uuid === lead.uuid);
        if (index === -1) {
          lead.event_id = null;
          lead.uploaded_at = null;
          leads.push(lead);
        } else {
          leads[index] = lead;
        }
        return this.saveLeads(leads)
          .then(() => {
            return lead;
          })
      })
  },
  saveLeads(leads) {
    /*return new Promise((resolve, reject) => {
      locker.add(KEY_LEADS, leads);
      return resolve(leads);
    })*/
    locker.add(KEY_LEADS, leads);
    return this.getLeads();
  },
  deleteLead(lead) {
    lead.deleted_at = moment.utc().toISOString();
    return this.updateLead(lead);
  }
}
