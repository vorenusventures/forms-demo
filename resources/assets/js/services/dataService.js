import _ from 'lodash';

const states = [
  {
    "name": "Alabama",
    "id": "AL"
  },
  {
    "name": "Alaska",
    "id": "AK"
  },
  /*{
    "name": "American Samoa",
    "id": "AS"
  },*/
  {
    "name": "Arizona",
    "id": "AZ"
  },
  {
    "name": "Arkansas",
    "id": "AR"
  },
  {
    "name": "California",
    "id": "CA"
  },
  {
    "name": "Colorado",
    "id": "CO"
  },
  {
    "name": "Connecticut",
    "id": "CT"
  },
  {
    "name": "Delaware",
    "id": "DE"
  },
  {
    "name": "District Of Columbia",
    "id": "DC"
  },
  /*{
    "name": "Federated States Of Micronesia",
    "id": "FM"
  },*/
  {
    "name": "Florida",
    "id": "FL"
  },
  {
    "name": "Georgia",
    "id": "GA"
  },
  {
    "name": "Guam",
    "id": "GU"
  },
  {
    "name": "Hawaii",
    "id": "HI"
  },
  {
    "name": "Idaho",
    "id": "ID"
  },
  {
    "name": "Illinois",
    "id": "IL"
  },
  {
    "name": "Indiana",
    "id": "IN"
  },
  {
    "name": "Iowa",
    "id": "IA"
  },
  {
    "name": "Kansas",
    "id": "KS"
  },
  {
    "name": "Kentucky",
    "id": "KY"
  },
  {
    "name": "Louisiana",
    "id": "LA"
  },
  {
    "name": "Maine",
    "id": "ME"
  },
  {
    "name": "Marshall Islands",
    "id": "MH"
  },
  {
    "name": "Maryland",
    "id": "MD"
  },
  {
    "name": "Massachusetts",
    "id": "MA"
  },
  {
    "name": "Michigan",
    "id": "MI"
  },
  {
    "name": "Minnesota",
    "id": "MN"
  },
  {
    "name": "Mississippi",
    "id": "MS"
  },
  {
    "name": "Missouri",
    "id": "MO"
  },
  {
    "name": "Montana",
    "id": "MT"
  },
  {
    "name": "Nebraska",
    "id": "NE"
  },
  {
    "name": "Nevada",
    "id": "NV"
  },
  {
    "name": "New Hampshire",
    "id": "NH"
  },
  {
    "name": "New Jersey",
    "id": "NJ"
  },
  {
    "name": "New Mexico",
    "id": "NM"
  },
  {
    "name": "New York",
    "id": "NY"
  },
  {
    "name": "North Carolina",
    "id": "NC"
  },
  {
    "name": "North Dakota",
    "id": "ND"
  },
  /*{
    "name": "Northern Mariana Islands",
    "id": "MP"
  },*/
  {
    "name": "Ohio",
    "id": "OH"
  },
  {
    "name": "Oklahoma",
    "id": "OK"
  },
  {
    "name": "Oregon",
    "id": "OR"
  },
  {
    "name": "Palau",
    "id": "PW"
  },
  {
    "name": "Pennsylvania",
    "id": "PA"
  },
  {
    "name": "Puerto Rico",
    "id": "PR"
  },
  {
    "name": "Rhode Island",
    "id": "RI"
  },
  {
    "name": "South Carolina",
    "id": "SC"
  },
  {
    "name": "South Dakota",
    "id": "SD"
  },
  {
    "name": "Tennessee",
    "id": "TN"
  },
  {
    "name": "Texas",
    "id": "TX"
  },
  {
    "name": "Utah",
    "id": "UT"
  },
  {
    "name": "Vermont",
    "id": "VT"
  },
  /*{
    "name": "Virgin Islands",
    "id": "VI"
  },*/
  {
    "name": "Virginia",
    "id": "VA"
  },
  {
    "name": "Washington",
    "id": "WA"
  },
  {
    "name": "West Virginia",
    "id": "WV"
  },
  {
    "name": "Wisconsin",
    "id": "WI"
  },
  {
    "name": "Wyoming",
    "id": "WY"
  }
];

// https://github.com/lukes/ISO-3166-Countries-with-Regional-Codes
const countries = [
  {
    "name": "Afghanistan",
    "alpha2": "AF",
    "alpha3": "AFG",
    "country_code": "004",
    "iso_3166_2": "ISO 3166-2:AF",
    "region": "Asia",
    "sub_region": "Southern Asia",
    "region_code": "142",
    "sub_region_code": "034"
  },
  {
    "name": "Åland Islands",
    "alpha2": "AX",
    "alpha3": "ALA",
    "country_code": "248",
    "iso_3166_2": "ISO 3166-2:AX",
    "sub_region_code": "154",
    "region_code": "150",
    "sub_region": "Northern Europe",
    "region": "Europe"
  },
  {
    "name": "Albania",
    "alpha2": "AL",
    "alpha3": "ALB",
    "country_code": "008",
    "iso_3166_2": "ISO 3166-2:AL",
    "region": "Europe",
    "sub_region": "Southern Europe",
    "region_code": "150",
    "sub_region_code": "039"
  },
  {
    "name": "Algeria",
    "alpha2": "DZ",
    "alpha3": "DZA",
    "country_code": "012",
    "iso_3166_2": "ISO 3166-2:DZ",
    "region": "Africa",
    "sub_region": "Northern Africa",
    "region_code": "002",
    "sub_region_code": "015"
  },
  {
    "name": "American Samoa",
    "alpha2": "AS",
    "alpha3": "ASM",
    "country_code": "016",
    "iso_3166_2": "ISO 3166-2:AS",
    "region": "Oceania",
    "sub_region": "Polynesia",
    "region_code": "009",
    "sub_region_code": "061"
  },
  {
    "name": "Andorra",
    "alpha2": "AD",
    "alpha3": "AND",
    "country_code": "020",
    "iso_3166_2": "ISO 3166-2:AD",
    "region": "Europe",
    "sub_region": "Southern Europe",
    "region_code": "150",
    "sub_region_code": "039"
  },
  {
    "name": "Angola",
    "alpha2": "AO",
    "alpha3": "AGO",
    "country_code": "024",
    "iso_3166_2": "ISO 3166-2:AO",
    "region": "Africa",
    "sub_region": "Middle Africa",
    "region_code": "002",
    "sub_region_code": "017"
  },
  {
    "name": "Anguilla",
    "alpha2": "AI",
    "alpha3": "AIA",
    "country_code": "660",
    "iso_3166_2": "ISO 3166-2:AI",
    "region": "Americas",
    "sub_region": "Caribbean",
    "region_code": "019",
    "sub_region_code": "029"
  },
  {
    "name": "Antarctica",
    "alpha2": "AQ",
    "alpha3": "ATA",
    "country_code": "010",
    "iso_3166_2": "ISO 3166-2:AQ",
    "sub_region_code": null,
    "region_code": null,
    "sub_region": null,
    "region": null
  },
  {
    "name": "Antigua and Barbuda",
    "alpha2": "AG",
    "alpha3": "ATG",
    "country_code": "028",
    "iso_3166_2": "ISO 3166-2:AG",
    "region": "Americas",
    "sub_region": "Caribbean",
    "region_code": "019",
    "sub_region_code": "029"
  },
  {
    "name": "Argentina",
    "alpha2": "AR",
    "alpha3": "ARG",
    "country_code": "032",
    "iso_3166_2": "ISO 3166-2:AR",
    "region": "Americas",
    "sub_region": "South America",
    "region_code": "019",
    "sub_region_code": "005"
  },
  {
    "name": "Armenia",
    "alpha2": "AM",
    "alpha3": "ARM",
    "country_code": "051",
    "iso_3166_2": "ISO 3166-2:AM",
    "region": "Asia",
    "sub_region": "Western Asia",
    "region_code": "142",
    "sub_region_code": "145"
  },
  {
    "name": "Aruba",
    "alpha2": "AW",
    "alpha3": "ABW",
    "country_code": "533",
    "iso_3166_2": "ISO 3166-2:AW",
    "region": "Americas",
    "sub_region": "Caribbean",
    "region_code": "019",
    "sub_region_code": "029"
  },
  {
    "name": "Australia",
    "alpha2": "AU",
    "alpha3": "AUS",
    "country_code": "036",
    "iso_3166_2": "ISO 3166-2:AU",
    "region": "Oceania",
    "sub_region": "Australia and New Zealand",
    "region_code": "009",
    "sub_region_code": "053"
  },
  {
    "name": "Austria",
    "alpha2": "AT",
    "alpha3": "AUT",
    "country_code": "040",
    "iso_3166_2": "ISO 3166-2:AT",
    "region": "Europe",
    "sub_region": "Western Europe",
    "region_code": "150",
    "sub_region_code": "155"
  },
  {
    "name": "Azerbaijan",
    "alpha2": "AZ",
    "alpha3": "AZE",
    "country_code": "031",
    "iso_3166_2": "ISO 3166-2:AZ",
    "region": "Asia",
    "sub_region": "Western Asia",
    "region_code": "142",
    "sub_region_code": "145"
  },
  {
    "name": "Bahamas",
    "alpha2": "BS",
    "alpha3": "BHS",
    "country_code": "044",
    "iso_3166_2": "ISO 3166-2:BS",
    "region": "Americas",
    "sub_region": "Caribbean",
    "region_code": "019",
    "sub_region_code": "029"
  },
  {
    "name": "Bahrain",
    "alpha2": "BH",
    "alpha3": "BHR",
    "country_code": "048",
    "iso_3166_2": "ISO 3166-2:BH",
    "region": "Asia",
    "sub_region": "Western Asia",
    "region_code": "142",
    "sub_region_code": "145"
  },
  {
    "name": "Bangladesh",
    "alpha2": "BD",
    "alpha3": "BGD",
    "country_code": "050",
    "iso_3166_2": "ISO 3166-2:BD",
    "region": "Asia",
    "sub_region": "Southern Asia",
    "region_code": "142",
    "sub_region_code": "034"
  },
  {
    "name": "Barbados",
    "alpha2": "BB",
    "alpha3": "BRB",
    "country_code": "052",
    "iso_3166_2": "ISO 3166-2:BB",
    "region": "Americas",
    "sub_region": "Caribbean",
    "region_code": "019",
    "sub_region_code": "029"
  },
  {
    "name": "Belarus",
    "alpha2": "BY",
    "alpha3": "BLR",
    "country_code": "112",
    "iso_3166_2": "ISO 3166-2:BY",
    "region": "Europe",
    "sub_region": "Eastern Europe",
    "region_code": "150",
    "sub_region_code": "151"
  },
  {
    "name": "Belgium",
    "alpha2": "BE",
    "alpha3": "BEL",
    "country_code": "056",
    "iso_3166_2": "ISO 3166-2:BE",
    "region": "Europe",
    "sub_region": "Western Europe",
    "region_code": "150",
    "sub_region_code": "155"
  },
  {
    "name": "Belize",
    "alpha2": "BZ",
    "alpha3": "BLZ",
    "country_code": "084",
    "iso_3166_2": "ISO 3166-2:BZ",
    "region": "Americas",
    "sub_region": "Central America",
    "region_code": "019",
    "sub_region_code": "013"
  },
  {
    "name": "Benin",
    "alpha2": "BJ",
    "alpha3": "BEN",
    "country_code": "204",
    "iso_3166_2": "ISO 3166-2:BJ",
    "region": "Africa",
    "sub_region": "Western Africa",
    "region_code": "002",
    "sub_region_code": "011"
  },
  {
    "name": "Bermuda",
    "alpha2": "BM",
    "alpha3": "BMU",
    "country_code": "060",
    "iso_3166_2": "ISO 3166-2:BM",
    "region": "Americas",
    "sub_region": "Northern America",
    "region_code": "019",
    "sub_region_code": "021"
  },
  {
    "name": "Bhutan",
    "alpha2": "BT",
    "alpha3": "BTN",
    "country_code": "064",
    "iso_3166_2": "ISO 3166-2:BT",
    "region": "Asia",
    "sub_region": "Southern Asia",
    "region_code": "142",
    "sub_region_code": "034"
  },
  {
    "name": "Bolivia (Plurinational State of)",
    "alpha2": "BO",
    "alpha3": "BOL",
    "country_code": "068",
    "iso_3166_2": "ISO 3166-2:BO",
    "region": "Americas",
    "sub_region": "South America",
    "region_code": "019",
    "sub_region_code": "005"
  },
  {
    "name": "Bonaire, Sint Eustatius and Saba",
    "alpha2": "BQ",
    "alpha3": "BES",
    "country_code": "535",
    "iso_3166_2": "ISO 3166-2:BQ",
    "region": "Americas",
    "sub_region": "Caribbean",
    "region_code": "019",
    "sub_region_code": "029"
  },
  {
    "name": "Bosnia and Herzegovina",
    "alpha2": "BA",
    "alpha3": "BIH",
    "country_code": "070",
    "iso_3166_2": "ISO 3166-2:BA",
    "region": "Europe",
    "sub_region": "Southern Europe",
    "region_code": "150",
    "sub_region_code": "039"
  },
  {
    "name": "Botswana",
    "alpha2": "BW",
    "alpha3": "BWA",
    "country_code": "072",
    "iso_3166_2": "ISO 3166-2:BW",
    "region": "Africa",
    "sub_region": "Southern Africa",
    "region_code": "002",
    "sub_region_code": "018"
  },
  {
    "name": "Bouvet Island",
    "alpha2": "BV",
    "alpha3": "BVT",
    "country_code": "074",
    "iso_3166_2": "ISO 3166-2:BV",
    "sub_region_code": null,
    "region_code": null,
    "sub_region": null,
    "region": null
  },
  {
    "name": "Brazil",
    "alpha2": "BR",
    "alpha3": "BRA",
    "country_code": "076",
    "iso_3166_2": "ISO 3166-2:BR",
    "region": "Americas",
    "sub_region": "South America",
    "region_code": "019",
    "sub_region_code": "005"
  },
  {
    "name": "British Indian Ocean Territory",
    "alpha2": "IO",
    "alpha3": "IOT",
    "country_code": "086",
    "iso_3166_2": "ISO 3166-2:IO",
    "sub_region_code": null,
    "region_code": null,
    "sub_region": null,
    "region": null
  },
  {
    "name": "Brunei Darussalam",
    "alpha2": "BN",
    "alpha3": "BRN",
    "country_code": "096",
    "iso_3166_2": "ISO 3166-2:BN",
    "region": "Asia",
    "sub_region": "South-Eastern Asia",
    "region_code": "142",
    "sub_region_code": "035"
  },
  {
    "name": "Bulgaria",
    "alpha2": "BG",
    "alpha3": "BGR",
    "country_code": "100",
    "iso_3166_2": "ISO 3166-2:BG",
    "region": "Europe",
    "sub_region": "Eastern Europe",
    "region_code": "150",
    "sub_region_code": "151"
  },
  {
    "name": "Burkina Faso",
    "alpha2": "BF",
    "alpha3": "BFA",
    "country_code": "854",
    "iso_3166_2": "ISO 3166-2:BF",
    "region": "Africa",
    "sub_region": "Western Africa",
    "region_code": "002",
    "sub_region_code": "011"
  },
  {
    "name": "Burundi",
    "alpha2": "BI",
    "alpha3": "BDI",
    "country_code": "108",
    "iso_3166_2": "ISO 3166-2:BI",
    "region": "Africa",
    "sub_region": "Eastern Africa",
    "region_code": "002",
    "sub_region_code": "014"
  },
  {
    "name": "Cambodia",
    "alpha2": "KH",
    "alpha3": "KHM",
    "country_code": "116",
    "iso_3166_2": "ISO 3166-2:KH",
    "region": "Asia",
    "sub_region": "South-Eastern Asia",
    "region_code": "142",
    "sub_region_code": "035"
  },
  {
    "name": "Cameroon",
    "alpha2": "CM",
    "alpha3": "CMR",
    "country_code": "120",
    "iso_3166_2": "ISO 3166-2:CM",
    "region": "Africa",
    "sub_region": "Middle Africa",
    "region_code": "002",
    "sub_region_code": "017"
  },
  {
    "name": "Canada",
    "alpha2": "CA",
    "alpha3": "CAN",
    "country_code": "124",
    "iso_3166_2": "ISO 3166-2:CA",
    "region": "Americas",
    "sub_region": "Northern America",
    "region_code": "019",
    "sub_region_code": "021"
  },
  {
    "name": "Cabo Verde",
    "alpha2": "CV",
    "alpha3": "CPV",
    "country_code": "132",
    "iso_3166_2": "ISO 3166-2:CV",
    "region": "Africa",
    "sub_region": "Western Africa",
    "region_code": "002",
    "sub_region_code": "011"
  },
  {
    "name": "Cayman Islands",
    "alpha2": "KY",
    "alpha3": "CYM",
    "country_code": "136",
    "iso_3166_2": "ISO 3166-2:KY",
    "region": "Americas",
    "sub_region": "Caribbean",
    "region_code": "019",
    "sub_region_code": "029"
  },
  {
    "name": "Central African Republic",
    "alpha2": "CF",
    "alpha3": "CAF",
    "country_code": "140",
    "iso_3166_2": "ISO 3166-2:CF",
    "region": "Africa",
    "sub_region": "Middle Africa",
    "region_code": "002",
    "sub_region_code": "017"
  },
  {
    "name": "Chad",
    "alpha2": "TD",
    "alpha3": "TCD",
    "country_code": "148",
    "iso_3166_2": "ISO 3166-2:TD",
    "region": "Africa",
    "sub_region": "Middle Africa",
    "region_code": "002",
    "sub_region_code": "017"
  },
  {
    "name": "Chile",
    "alpha2": "CL",
    "alpha3": "CHL",
    "country_code": "152",
    "iso_3166_2": "ISO 3166-2:CL",
    "region": "Americas",
    "sub_region": "South America",
    "region_code": "019",
    "sub_region_code": "005"
  },
  {
    "name": "China",
    "alpha2": "CN",
    "alpha3": "CHN",
    "country_code": "156",
    "iso_3166_2": "ISO 3166-2:CN",
    "region": "Asia",
    "sub_region": "Eastern Asia",
    "region_code": "142",
    "sub_region_code": "030"
  },
  {
    "name": "Christmas Island",
    "alpha2": "CX",
    "alpha3": "CXR",
    "country_code": "162",
    "iso_3166_2": "ISO 3166-2:CX",
    "sub_region_code": null,
    "region_code": null,
    "sub_region": null,
    "region": null
  },
  {
    "name": "Cocos (Keeling) Islands",
    "alpha2": "CC",
    "alpha3": "CCK",
    "country_code": "166",
    "iso_3166_2": "ISO 3166-2:CC",
    "sub_region_code": null,
    "region_code": null,
    "sub_region": null,
    "region": null
  },
  {
    "name": "Colombia",
    "alpha2": "CO",
    "alpha3": "COL",
    "country_code": "170",
    "iso_3166_2": "ISO 3166-2:CO",
    "region": "Americas",
    "sub_region": "South America",
    "region_code": "019",
    "sub_region_code": "005"
  },
  {
    "name": "Comoros",
    "alpha2": "KM",
    "alpha3": "COM",
    "country_code": "174",
    "iso_3166_2": "ISO 3166-2:KM",
    "region": "Africa",
    "sub_region": "Eastern Africa",
    "region_code": "002",
    "sub_region_code": "014"
  },
  {
    "name": "Congo",
    "alpha2": "CG",
    "alpha3": "COG",
    "country_code": "178",
    "iso_3166_2": "ISO 3166-2:CG",
    "region": "Africa",
    "sub_region": "Middle Africa",
    "region_code": "002",
    "sub_region_code": "017"
  },
  {
    "name": "Congo (Democratic Republic of the)",
    "alpha2": "CD",
    "alpha3": "COD",
    "country_code": "180",
    "iso_3166_2": "ISO 3166-2:CD",
    "region": "Africa",
    "sub_region": "Middle Africa",
    "region_code": "002",
    "sub_region_code": "017"
  },
  {
    "name": "Cook Islands",
    "alpha2": "CK",
    "alpha3": "COK",
    "country_code": "184",
    "iso_3166_2": "ISO 3166-2:CK",
    "region": "Oceania",
    "sub_region": "Polynesia",
    "region_code": "009",
    "sub_region_code": "061"
  },
  {
    "name": "Costa Rica",
    "alpha2": "CR",
    "alpha3": "CRI",
    "country_code": "188",
    "iso_3166_2": "ISO 3166-2:CR",
    "region": "Americas",
    "sub_region": "Central America",
    "region_code": "019",
    "sub_region_code": "013"
  },
  {
    "name": "Côte d'Ivoire",
    "alpha2": "CI",
    "alpha3": "CIV",
    "country_code": "384",
    "iso_3166_2": "ISO 3166-2:CI",
    "region": "Africa",
    "sub_region": "Western Africa",
    "region_code": "002",
    "sub_region_code": "011"
  },
  {
    "name": "Croatia",
    "alpha2": "HR",
    "alpha3": "HRV",
    "country_code": "191",
    "iso_3166_2": "ISO 3166-2:HR",
    "region": "Europe",
    "sub_region": "Southern Europe",
    "region_code": "150",
    "sub_region_code": "039"
  },
  {
    "name": "Cuba",
    "alpha2": "CU",
    "alpha3": "CUB",
    "country_code": "192",
    "iso_3166_2": "ISO 3166-2:CU",
    "region": "Americas",
    "sub_region": "Caribbean",
    "region_code": "019",
    "sub_region_code": "029"
  },
  {
    "name": "Curaçao",
    "alpha2": "CW",
    "alpha3": "CUW",
    "country_code": "531",
    "iso_3166_2": "ISO 3166-2:CW",
    "region": "Americas",
    "sub_region": "Caribbean",
    "region_code": "019",
    "sub_region_code": "029"
  },
  {
    "name": "Cyprus",
    "alpha2": "CY",
    "alpha3": "CYP",
    "country_code": "196",
    "iso_3166_2": "ISO 3166-2:CY",
    "region": "Asia",
    "sub_region": "Western Asia",
    "region_code": "142",
    "sub_region_code": "145"
  },
  {
    "name": "Czech Republic",
    "alpha2": "CZ",
    "alpha3": "CZE",
    "country_code": "203",
    "iso_3166_2": "ISO 3166-2:CZ",
    "region": "Europe",
    "sub_region": "Eastern Europe",
    "region_code": "150",
    "sub_region_code": "151"
  },
  {
    "name": "Denmark",
    "alpha2": "DK",
    "alpha3": "DNK",
    "country_code": "208",
    "iso_3166_2": "ISO 3166-2:DK",
    "region": "Europe",
    "sub_region": "Northern Europe",
    "region_code": "150",
    "sub_region_code": "154"
  },
  {
    "name": "Djibouti",
    "alpha2": "DJ",
    "alpha3": "DJI",
    "country_code": "262",
    "iso_3166_2": "ISO 3166-2:DJ",
    "region": "Africa",
    "sub_region": "Eastern Africa",
    "region_code": "002",
    "sub_region_code": "014"
  },
  {
    "name": "Dominica",
    "alpha2": "DM",
    "alpha3": "DMA",
    "country_code": "212",
    "iso_3166_2": "ISO 3166-2:DM",
    "region": "Americas",
    "sub_region": "Caribbean",
    "region_code": "019",
    "sub_region_code": "029"
  },
  {
    "name": "Dominican Republic",
    "alpha2": "DO",
    "alpha3": "DOM",
    "country_code": "214",
    "iso_3166_2": "ISO 3166-2:DO",
    "region": "Americas",
    "sub_region": "Caribbean",
    "region_code": "019",
    "sub_region_code": "029"
  },
  {
    "name": "Ecuador",
    "alpha2": "EC",
    "alpha3": "ECU",
    "country_code": "218",
    "iso_3166_2": "ISO 3166-2:EC",
    "region": "Americas",
    "sub_region": "South America",
    "region_code": "019",
    "sub_region_code": "005"
  },
  {
    "name": "Egypt",
    "alpha2": "EG",
    "alpha3": "EGY",
    "country_code": "818",
    "iso_3166_2": "ISO 3166-2:EG",
    "region": "Africa",
    "sub_region": "Northern Africa",
    "region_code": "002",
    "sub_region_code": "015"
  },
  {
    "name": "El Salvador",
    "alpha2": "SV",
    "alpha3": "SLV",
    "country_code": "222",
    "iso_3166_2": "ISO 3166-2:SV",
    "region": "Americas",
    "sub_region": "Central America",
    "region_code": "019",
    "sub_region_code": "013"
  },
  {
    "name": "Equatorial Guinea",
    "alpha2": "GQ",
    "alpha3": "GNQ",
    "country_code": "226",
    "iso_3166_2": "ISO 3166-2:GQ",
    "region": "Africa",
    "sub_region": "Middle Africa",
    "region_code": "002",
    "sub_region_code": "017"
  },
  {
    "name": "Eritrea",
    "alpha2": "ER",
    "alpha3": "ERI",
    "country_code": "232",
    "iso_3166_2": "ISO 3166-2:ER",
    "region": "Africa",
    "sub_region": "Eastern Africa",
    "region_code": "002",
    "sub_region_code": "014"
  },
  {
    "name": "Estonia",
    "alpha2": "EE",
    "alpha3": "EST",
    "country_code": "233",
    "iso_3166_2": "ISO 3166-2:EE",
    "region": "Europe",
    "sub_region": "Northern Europe",
    "region_code": "150",
    "sub_region_code": "154"
  },
  {
    "name": "Ethiopia",
    "alpha2": "ET",
    "alpha3": "ETH",
    "country_code": "231",
    "iso_3166_2": "ISO 3166-2:ET",
    "region": "Africa",
    "sub_region": "Eastern Africa",
    "region_code": "002",
    "sub_region_code": "014"
  },
  {
    "name": "Falkland Islands (Malvinas)",
    "alpha2": "FK",
    "alpha3": "FLK",
    "country_code": "238",
    "iso_3166_2": "ISO 3166-2:FK",
    "region": "Americas",
    "sub_region": "South America",
    "region_code": "019",
    "sub_region_code": "005"
  },
  {
    "name": "Faroe Islands",
    "alpha2": "FO",
    "alpha3": "FRO",
    "country_code": "234",
    "iso_3166_2": "ISO 3166-2:FO",
    "region": "Europe",
    "sub_region": "Northern Europe",
    "region_code": "150",
    "sub_region_code": "154"
  },
  {
    "name": "Fiji",
    "alpha2": "FJ",
    "alpha3": "FJI",
    "country_code": "242",
    "iso_3166_2": "ISO 3166-2:FJ",
    "region": "Oceania",
    "sub_region": "Melanesia",
    "region_code": "009",
    "sub_region_code": "054"
  },
  {
    "name": "Finland",
    "alpha2": "FI",
    "alpha3": "FIN",
    "country_code": "246",
    "iso_3166_2": "ISO 3166-2:FI",
    "region": "Europe",
    "sub_region": "Northern Europe",
    "region_code": "150",
    "sub_region_code": "154"
  },
  {
    "name": "France",
    "alpha2": "FR",
    "alpha3": "FRA",
    "country_code": "250",
    "iso_3166_2": "ISO 3166-2:FR",
    "region": "Europe",
    "sub_region": "Western Europe",
    "region_code": "150",
    "sub_region_code": "155"
  },
  {
    "name": "French Guiana",
    "alpha2": "GF",
    "alpha3": "GUF",
    "country_code": "254",
    "iso_3166_2": "ISO 3166-2:GF",
    "region": "Americas",
    "sub_region": "South America",
    "region_code": "019",
    "sub_region_code": "005"
  },
  {
    "name": "French Polynesia",
    "alpha2": "PF",
    "alpha3": "PYF",
    "country_code": "258",
    "iso_3166_2": "ISO 3166-2:PF",
    "region": "Oceania",
    "sub_region": "Polynesia",
    "region_code": "009",
    "sub_region_code": "061"
  },
  {
    "name": "French Southern Territories",
    "alpha2": "TF",
    "alpha3": "ATF",
    "country_code": "260",
    "iso_3166_2": "ISO 3166-2:TF",
    "sub_region_code": null,
    "region_code": null,
    "sub_region": null,
    "region": null
  },
  {
    "name": "Gabon",
    "alpha2": "GA",
    "alpha3": "GAB",
    "country_code": "266",
    "iso_3166_2": "ISO 3166-2:GA",
    "region": "Africa",
    "sub_region": "Middle Africa",
    "region_code": "002",
    "sub_region_code": "017"
  },
  {
    "name": "Gambia",
    "alpha2": "GM",
    "alpha3": "GMB",
    "country_code": "270",
    "iso_3166_2": "ISO 3166-2:GM",
    "region": "Africa",
    "sub_region": "Western Africa",
    "region_code": "002",
    "sub_region_code": "011"
  },
  {
    "name": "Georgia",
    "alpha2": "GE",
    "alpha3": "GEO",
    "country_code": "268",
    "iso_3166_2": "ISO 3166-2:GE",
    "region": "Asia",
    "sub_region": "Western Asia",
    "region_code": "142",
    "sub_region_code": "145"
  },
  {
    "name": "Germany",
    "alpha2": "DE",
    "alpha3": "DEU",
    "country_code": "276",
    "iso_3166_2": "ISO 3166-2:DE",
    "region": "Europe",
    "sub_region": "Western Europe",
    "region_code": "150",
    "sub_region_code": "155"
  },
  {
    "name": "Ghana",
    "alpha2": "GH",
    "alpha3": "GHA",
    "country_code": "288",
    "iso_3166_2": "ISO 3166-2:GH",
    "region": "Africa",
    "sub_region": "Western Africa",
    "region_code": "002",
    "sub_region_code": "011"
  },
  {
    "name": "Gibraltar",
    "alpha2": "GI",
    "alpha3": "GIB",
    "country_code": "292",
    "iso_3166_2": "ISO 3166-2:GI",
    "region": "Europe",
    "sub_region": "Southern Europe",
    "region_code": "150",
    "sub_region_code": "039"
  },
  {
    "name": "Greece",
    "alpha2": "GR",
    "alpha3": "GRC",
    "country_code": "300",
    "iso_3166_2": "ISO 3166-2:GR",
    "region": "Europe",
    "sub_region": "Southern Europe",
    "region_code": "150",
    "sub_region_code": "039"
  },
  {
    "name": "Greenland",
    "alpha2": "GL",
    "alpha3": "GRL",
    "country_code": "304",
    "iso_3166_2": "ISO 3166-2:GL",
    "region": "Americas",
    "sub_region": "Northern America",
    "region_code": "019",
    "sub_region_code": "021"
  },
  {
    "name": "Grenada",
    "alpha2": "GD",
    "alpha3": "GRD",
    "country_code": "308",
    "iso_3166_2": "ISO 3166-2:GD",
    "region": "Americas",
    "sub_region": "Caribbean",
    "region_code": "019",
    "sub_region_code": "029"
  },
  {
    "name": "Guadeloupe",
    "alpha2": "GP",
    "alpha3": "GLP",
    "country_code": "312",
    "iso_3166_2": "ISO 3166-2:GP",
    "region": "Americas",
    "sub_region": "Caribbean",
    "region_code": "019",
    "sub_region_code": "029"
  },
  {
    "name": "Guam",
    "alpha2": "GU",
    "alpha3": "GUM",
    "country_code": "316",
    "iso_3166_2": "ISO 3166-2:GU",
    "region": "Oceania",
    "sub_region": "Micronesia",
    "region_code": "009",
    "sub_region_code": "057"
  },
  {
    "name": "Guatemala",
    "alpha2": "GT",
    "alpha3": "GTM",
    "country_code": "320",
    "iso_3166_2": "ISO 3166-2:GT",
    "region": "Americas",
    "sub_region": "Central America",
    "region_code": "019",
    "sub_region_code": "013"
  },
  {
    "name": "Guernsey",
    "alpha2": "GG",
    "alpha3": "GGY",
    "country_code": "831",
    "iso_3166_2": "ISO 3166-2:GG",
    "region": "Europe",
    "sub_region": "Northern Europe",
    "region_code": "150",
    "sub_region_code": "154"
  },
  {
    "name": "Guinea",
    "alpha2": "GN",
    "alpha3": "GIN",
    "country_code": "324",
    "iso_3166_2": "ISO 3166-2:GN",
    "region": "Africa",
    "sub_region": "Western Africa",
    "region_code": "002",
    "sub_region_code": "011"
  },
  {
    "name": "Guinea-Bissau",
    "alpha2": "GW",
    "alpha3": "GNB",
    "country_code": "624",
    "iso_3166_2": "ISO 3166-2:GW",
    "region": "Africa",
    "sub_region": "Western Africa",
    "region_code": "002",
    "sub_region_code": "011"
  },
  {
    "name": "Guyana",
    "alpha2": "GY",
    "alpha3": "GUY",
    "country_code": "328",
    "iso_3166_2": "ISO 3166-2:GY",
    "region": "Americas",
    "sub_region": "South America",
    "region_code": "019",
    "sub_region_code": "005"
  },
  {
    "name": "Haiti",
    "alpha2": "HT",
    "alpha3": "HTI",
    "country_code": "332",
    "iso_3166_2": "ISO 3166-2:HT",
    "region": "Americas",
    "sub_region": "Caribbean",
    "region_code": "019",
    "sub_region_code": "029"
  },
  {
    "name": "Heard Island and McDonald Islands",
    "alpha2": "HM",
    "alpha3": "HMD",
    "country_code": "334",
    "iso_3166_2": "ISO 3166-2:HM",
    "sub_region_code": null,
    "region_code": null,
    "sub_region": null,
    "region": null
  },
  {
    "name": "Holy See",
    "alpha2": "VA",
    "alpha3": "VAT",
    "country_code": "336",
    "iso_3166_2": "ISO 3166-2:VA",
    "region": "Europe",
    "sub_region": "Southern Europe",
    "region_code": "150",
    "sub_region_code": "039"
  },
  {
    "name": "Honduras",
    "alpha2": "HN",
    "alpha3": "HND",
    "country_code": "340",
    "iso_3166_2": "ISO 3166-2:HN",
    "region": "Americas",
    "sub_region": "Central America",
    "region_code": "019",
    "sub_region_code": "013"
  },
  {
    "name": "Hong Kong",
    "alpha2": "HK",
    "alpha3": "HKG",
    "country_code": "344",
    "iso_3166_2": "ISO 3166-2:HK",
    "region": "Asia",
    "sub_region": "Eastern Asia",
    "region_code": "142",
    "sub_region_code": "030"
  },
  {
    "name": "Hungary",
    "alpha2": "HU",
    "alpha3": "HUN",
    "country_code": "348",
    "iso_3166_2": "ISO 3166-2:HU",
    "region": "Europe",
    "sub_region": "Eastern Europe",
    "region_code": "150",
    "sub_region_code": "151"
  },
  {
    "name": "Iceland",
    "alpha2": "IS",
    "alpha3": "ISL",
    "country_code": "352",
    "iso_3166_2": "ISO 3166-2:IS",
    "region": "Europe",
    "sub_region": "Northern Europe",
    "region_code": "150",
    "sub_region_code": "154"
  },
  {
    "name": "India",
    "alpha2": "IN",
    "alpha3": "IND",
    "country_code": "356",
    "iso_3166_2": "ISO 3166-2:IN",
    "region": "Asia",
    "sub_region": "Southern Asia",
    "region_code": "142",
    "sub_region_code": "034"
  },
  {
    "name": "Indonesia",
    "alpha2": "ID",
    "alpha3": "IDN",
    "country_code": "360",
    "iso_3166_2": "ISO 3166-2:ID",
    "region": "Asia",
    "sub_region": "South-Eastern Asia",
    "region_code": "142",
    "sub_region_code": "035"
  },
  {
    "name": "Iran (Islamic Republic of)",
    "alpha2": "IR",
    "alpha3": "IRN",
    "country_code": "364",
    "iso_3166_2": "ISO 3166-2:IR",
    "region": "Asia",
    "sub_region": "Southern Asia",
    "region_code": "142",
    "sub_region_code": "034"
  },
  {
    "name": "Iraq",
    "alpha2": "IQ",
    "alpha3": "IRQ",
    "country_code": "368",
    "iso_3166_2": "ISO 3166-2:IQ",
    "region": "Asia",
    "sub_region": "Western Asia",
    "region_code": "142",
    "sub_region_code": "145"
  },
  {
    "name": "Ireland",
    "alpha2": "IE",
    "alpha3": "IRL",
    "country_code": "372",
    "iso_3166_2": "ISO 3166-2:IE",
    "region": "Europe",
    "sub_region": "Northern Europe",
    "region_code": "150",
    "sub_region_code": "154"
  },
  {
    "name": "Isle of Man",
    "alpha2": "IM",
    "alpha3": "IMN",
    "country_code": "833",
    "iso_3166_2": "ISO 3166-2:IM",
    "region": "Europe",
    "sub_region": "Northern Europe",
    "region_code": "150",
    "sub_region_code": "154"
  },
  {
    "name": "Israel",
    "alpha2": "IL",
    "alpha3": "ISR",
    "country_code": "376",
    "iso_3166_2": "ISO 3166-2:IL",
    "region": "Asia",
    "sub_region": "Western Asia",
    "region_code": "142",
    "sub_region_code": "145"
  },
  {
    "name": "Italy",
    "alpha2": "IT",
    "alpha3": "ITA",
    "country_code": "380",
    "iso_3166_2": "ISO 3166-2:IT",
    "region": "Europe",
    "sub_region": "Southern Europe",
    "region_code": "150",
    "sub_region_code": "039"
  },
  {
    "name": "Jamaica",
    "alpha2": "JM",
    "alpha3": "JAM",
    "country_code": "388",
    "iso_3166_2": "ISO 3166-2:JM",
    "region": "Americas",
    "sub_region": "Caribbean",
    "region_code": "019",
    "sub_region_code": "029"
  },
  {
    "name": "Japan",
    "alpha2": "JP",
    "alpha3": "JPN",
    "country_code": "392",
    "iso_3166_2": "ISO 3166-2:JP",
    "region": "Asia",
    "sub_region": "Eastern Asia",
    "region_code": "142",
    "sub_region_code": "030"
  },
  {
    "name": "Jersey",
    "alpha2": "JE",
    "alpha3": "JEY",
    "country_code": "832",
    "iso_3166_2": "ISO 3166-2:JE",
    "region": "Europe",
    "sub_region": "Northern Europe",
    "region_code": "150",
    "sub_region_code": "154"
  },
  {
    "name": "Jordan",
    "alpha2": "JO",
    "alpha3": "JOR",
    "country_code": "400",
    "iso_3166_2": "ISO 3166-2:JO",
    "region": "Asia",
    "sub_region": "Western Asia",
    "region_code": "142",
    "sub_region_code": "145"
  },
  {
    "name": "Kazakhstan",
    "alpha2": "KZ",
    "alpha3": "KAZ",
    "country_code": "398",
    "iso_3166_2": "ISO 3166-2:KZ",
    "region": "Asia",
    "sub_region": "Central Asia",
    "region_code": "142",
    "sub_region_code": "143"
  },
  {
    "name": "Kenya",
    "alpha2": "KE",
    "alpha3": "KEN",
    "country_code": "404",
    "iso_3166_2": "ISO 3166-2:KE",
    "region": "Africa",
    "sub_region": "Eastern Africa",
    "region_code": "002",
    "sub_region_code": "014"
  },
  {
    "name": "Kiribati",
    "alpha2": "KI",
    "alpha3": "KIR",
    "country_code": "296",
    "iso_3166_2": "ISO 3166-2:KI",
    "region": "Oceania",
    "sub_region": "Micronesia",
    "region_code": "009",
    "sub_region_code": "057"
  },
  {
    "name": "Korea (Democratic People's Republic of)",
    "alpha2": "KP",
    "alpha3": "PRK",
    "country_code": "408",
    "iso_3166_2": "ISO 3166-2:KP",
    "region": "Asia",
    "sub_region": "Eastern Asia",
    "region_code": "142",
    "sub_region_code": "030"
  },
  {
    "name": "Korea (Republic of)",
    "alpha2": "KR",
    "alpha3": "KOR",
    "country_code": "410",
    "iso_3166_2": "ISO 3166-2:KR",
    "region": "Asia",
    "sub_region": "Eastern Asia",
    "region_code": "142",
    "sub_region_code": "030"
  },
  {
    "name": "Kuwait",
    "alpha2": "KW",
    "alpha3": "KWT",
    "country_code": "414",
    "iso_3166_2": "ISO 3166-2:KW",
    "region": "Asia",
    "sub_region": "Western Asia",
    "region_code": "142",
    "sub_region_code": "145"
  },
  {
    "name": "Kyrgyzstan",
    "alpha2": "KG",
    "alpha3": "KGZ",
    "country_code": "417",
    "iso_3166_2": "ISO 3166-2:KG",
    "region": "Asia",
    "sub_region": "Central Asia",
    "region_code": "142",
    "sub_region_code": "143"
  },
  {
    "name": "Lao People's Democratic Republic",
    "alpha2": "LA",
    "alpha3": "LAO",
    "country_code": "418",
    "iso_3166_2": "ISO 3166-2:LA",
    "region": "Asia",
    "sub_region": "South-Eastern Asia",
    "region_code": "142",
    "sub_region_code": "035"
  },
  {
    "name": "Latvia",
    "alpha2": "LV",
    "alpha3": "LVA",
    "country_code": "428",
    "iso_3166_2": "ISO 3166-2:LV",
    "region": "Europe",
    "sub_region": "Northern Europe",
    "region_code": "150",
    "sub_region_code": "154"
  },
  {
    "name": "Lebanon",
    "alpha2": "LB",
    "alpha3": "LBN",
    "country_code": "422",
    "iso_3166_2": "ISO 3166-2:LB",
    "region": "Asia",
    "sub_region": "Western Asia",
    "region_code": "142",
    "sub_region_code": "145"
  },
  {
    "name": "Lesotho",
    "alpha2": "LS",
    "alpha3": "LSO",
    "country_code": "426",
    "iso_3166_2": "ISO 3166-2:LS",
    "region": "Africa",
    "sub_region": "Southern Africa",
    "region_code": "002",
    "sub_region_code": "018"
  },
  {
    "name": "Liberia",
    "alpha2": "LR",
    "alpha3": "LBR",
    "country_code": "430",
    "iso_3166_2": "ISO 3166-2:LR",
    "region": "Africa",
    "sub_region": "Western Africa",
    "region_code": "002",
    "sub_region_code": "011"
  },
  {
    "name": "Libya",
    "alpha2": "LY",
    "alpha3": "LBY",
    "country_code": "434",
    "iso_3166_2": "ISO 3166-2:LY",
    "region": "Africa",
    "sub_region": "Northern Africa",
    "region_code": "002",
    "sub_region_code": "015"
  },
  {
    "name": "Liechtenstein",
    "alpha2": "LI",
    "alpha3": "LIE",
    "country_code": "438",
    "iso_3166_2": "ISO 3166-2:LI",
    "region": "Europe",
    "sub_region": "Western Europe",
    "region_code": "150",
    "sub_region_code": "155"
  },
  {
    "name": "Lithuania",
    "alpha2": "LT",
    "alpha3": "LTU",
    "country_code": "440",
    "iso_3166_2": "ISO 3166-2:LT",
    "region": "Europe",
    "sub_region": "Northern Europe",
    "region_code": "150",
    "sub_region_code": "154"
  },
  {
    "name": "Luxembourg",
    "alpha2": "LU",
    "alpha3": "LUX",
    "country_code": "442",
    "iso_3166_2": "ISO 3166-2:LU",
    "region": "Europe",
    "sub_region": "Western Europe",
    "region_code": "150",
    "sub_region_code": "155"
  },
  {
    "name": "Macao",
    "alpha2": "MO",
    "alpha3": "MAC",
    "country_code": "446",
    "iso_3166_2": "ISO 3166-2:MO",
    "region": "Asia",
    "sub_region": "Eastern Asia",
    "region_code": "142",
    "sub_region_code": "030"
  },
  {
    "name": "Macedonia (the former Yugoslav Republic of)",
    "alpha2": "MK",
    "alpha3": "MKD",
    "country_code": "807",
    "iso_3166_2": "ISO 3166-2:MK",
    "region": "Europe",
    "sub_region": "Southern Europe",
    "region_code": "150",
    "sub_region_code": "039"
  },
  {
    "name": "Madagascar",
    "alpha2": "MG",
    "alpha3": "MDG",
    "country_code": "450",
    "iso_3166_2": "ISO 3166-2:MG",
    "region": "Africa",
    "sub_region": "Eastern Africa",
    "region_code": "002",
    "sub_region_code": "014"
  },
  {
    "name": "Malawi",
    "alpha2": "MW",
    "alpha3": "MWI",
    "country_code": "454",
    "iso_3166_2": "ISO 3166-2:MW",
    "region": "Africa",
    "sub_region": "Eastern Africa",
    "region_code": "002",
    "sub_region_code": "014"
  },
  {
    "name": "Malaysia",
    "alpha2": "MY",
    "alpha3": "MYS",
    "country_code": "458",
    "iso_3166_2": "ISO 3166-2:MY",
    "region": "Asia",
    "sub_region": "South-Eastern Asia",
    "region_code": "142",
    "sub_region_code": "035"
  },
  {
    "name": "Maldives",
    "alpha2": "MV",
    "alpha3": "MDV",
    "country_code": "462",
    "iso_3166_2": "ISO 3166-2:MV",
    "region": "Asia",
    "sub_region": "Southern Asia",
    "region_code": "142",
    "sub_region_code": "034"
  },
  {
    "name": "Mali",
    "alpha2": "ML",
    "alpha3": "MLI",
    "country_code": "466",
    "iso_3166_2": "ISO 3166-2:ML",
    "region": "Africa",
    "sub_region": "Western Africa",
    "region_code": "002",
    "sub_region_code": "011"
  },
  {
    "name": "Malta",
    "alpha2": "MT",
    "alpha3": "MLT",
    "country_code": "470",
    "iso_3166_2": "ISO 3166-2:MT",
    "region": "Europe",
    "sub_region": "Southern Europe",
    "region_code": "150",
    "sub_region_code": "039"
  },
  {
    "name": "Marshall Islands",
    "alpha2": "MH",
    "alpha3": "MHL",
    "country_code": "584",
    "iso_3166_2": "ISO 3166-2:MH",
    "region": "Oceania",
    "sub_region": "Micronesia",
    "region_code": "009",
    "sub_region_code": "057"
  },
  {
    "name": "Martinique",
    "alpha2": "MQ",
    "alpha3": "MTQ",
    "country_code": "474",
    "iso_3166_2": "ISO 3166-2:MQ",
    "region": "Americas",
    "sub_region": "Caribbean",
    "region_code": "019",
    "sub_region_code": "029"
  },
  {
    "name": "Mauritania",
    "alpha2": "MR",
    "alpha3": "MRT",
    "country_code": "478",
    "iso_3166_2": "ISO 3166-2:MR",
    "region": "Africa",
    "sub_region": "Western Africa",
    "region_code": "002",
    "sub_region_code": "011"
  },
  {
    "name": "Mauritius",
    "alpha2": "MU",
    "alpha3": "MUS",
    "country_code": "480",
    "iso_3166_2": "ISO 3166-2:MU",
    "region": "Africa",
    "sub_region": "Eastern Africa",
    "region_code": "002",
    "sub_region_code": "014"
  },
  {
    "name": "Mayotte",
    "alpha2": "YT",
    "alpha3": "MYT",
    "country_code": "175",
    "iso_3166_2": "ISO 3166-2:YT",
    "region": "Africa",
    "sub_region": "Eastern Africa",
    "region_code": "002",
    "sub_region_code": "014"
  },
  {
    "name": "Mexico",
    "alpha2": "MX",
    "alpha3": "MEX",
    "country_code": "484",
    "iso_3166_2": "ISO 3166-2:MX",
    "region": "Americas",
    "sub_region": "Central America",
    "region_code": "019",
    "sub_region_code": "013"
  },
  {
    "name": "Micronesia (Federated States of)",
    "alpha2": "FM",
    "alpha3": "FSM",
    "country_code": "583",
    "iso_3166_2": "ISO 3166-2:FM",
    "region": "Oceania",
    "sub_region": "Micronesia",
    "region_code": "009",
    "sub_region_code": "057"
  },
  {
    "name": "Moldova (Republic of)",
    "alpha2": "MD",
    "alpha3": "MDA",
    "country_code": "498",
    "iso_3166_2": "ISO 3166-2:MD",
    "region": "Europe",
    "sub_region": "Eastern Europe",
    "region_code": "150",
    "sub_region_code": "151"
  },
  {
    "name": "Monaco",
    "alpha2": "MC",
    "alpha3": "MCO",
    "country_code": "492",
    "iso_3166_2": "ISO 3166-2:MC",
    "region": "Europe",
    "sub_region": "Western Europe",
    "region_code": "150",
    "sub_region_code": "155"
  },
  {
    "name": "Mongolia",
    "alpha2": "MN",
    "alpha3": "MNG",
    "country_code": "496",
    "iso_3166_2": "ISO 3166-2:MN",
    "region": "Asia",
    "sub_region": "Eastern Asia",
    "region_code": "142",
    "sub_region_code": "030"
  },
  {
    "name": "Montenegro",
    "alpha2": "ME",
    "alpha3": "MNE",
    "country_code": "499",
    "iso_3166_2": "ISO 3166-2:ME",
    "region": "Europe",
    "sub_region": "Southern Europe",
    "region_code": "150",
    "sub_region_code": "039"
  },
  {
    "name": "Montserrat",
    "alpha2": "MS",
    "alpha3": "MSR",
    "country_code": "500",
    "iso_3166_2": "ISO 3166-2:MS",
    "region": "Americas",
    "sub_region": "Caribbean",
    "region_code": "019",
    "sub_region_code": "029"
  },
  {
    "name": "Morocco",
    "alpha2": "MA",
    "alpha3": "MAR",
    "country_code": "504",
    "iso_3166_2": "ISO 3166-2:MA",
    "region": "Africa",
    "sub_region": "Northern Africa",
    "region_code": "002",
    "sub_region_code": "015"
  },
  {
    "name": "Mozambique",
    "alpha2": "MZ",
    "alpha3": "MOZ",
    "country_code": "508",
    "iso_3166_2": "ISO 3166-2:MZ",
    "region": "Africa",
    "sub_region": "Eastern Africa",
    "region_code": "002",
    "sub_region_code": "014"
  },
  {
    "name": "Myanmar",
    "alpha2": "MM",
    "alpha3": "MMR",
    "country_code": "104",
    "iso_3166_2": "ISO 3166-2:MM",
    "region": "Asia",
    "sub_region": "South-Eastern Asia",
    "region_code": "142",
    "sub_region_code": "035"
  },
  {
    "name": "Namibia",
    "alpha2": "NA",
    "alpha3": "NAM",
    "country_code": "516",
    "iso_3166_2": "ISO 3166-2:NA",
    "region": "Africa",
    "sub_region": "Southern Africa",
    "region_code": "002",
    "sub_region_code": "018"
  },
  {
    "name": "Nauru",
    "alpha2": "NR",
    "alpha3": "NRU",
    "country_code": "520",
    "iso_3166_2": "ISO 3166-2:NR",
    "region": "Oceania",
    "sub_region": "Micronesia",
    "region_code": "009",
    "sub_region_code": "057"
  },
  {
    "name": "Nepal",
    "alpha2": "NP",
    "alpha3": "NPL",
    "country_code": "524",
    "iso_3166_2": "ISO 3166-2:NP",
    "region": "Asia",
    "sub_region": "Southern Asia",
    "region_code": "142",
    "sub_region_code": "034"
  },
  {
    "name": "Netherlands",
    "alpha2": "NL",
    "alpha3": "NLD",
    "country_code": "528",
    "iso_3166_2": "ISO 3166-2:NL",
    "region": "Europe",
    "sub_region": "Western Europe",
    "region_code": "150",
    "sub_region_code": "155"
  },
  {
    "name": "New Caledonia",
    "alpha2": "NC",
    "alpha3": "NCL",
    "country_code": "540",
    "iso_3166_2": "ISO 3166-2:NC",
    "region": "Oceania",
    "sub_region": "Melanesia",
    "region_code": "009",
    "sub_region_code": "054"
  },
  {
    "name": "New Zealand",
    "alpha2": "NZ",
    "alpha3": "NZL",
    "country_code": "554",
    "iso_3166_2": "ISO 3166-2:NZ",
    "region": "Oceania",
    "sub_region": "Australia and New Zealand",
    "region_code": "009",
    "sub_region_code": "053"
  },
  {
    "name": "Nicaragua",
    "alpha2": "NI",
    "alpha3": "NIC",
    "country_code": "558",
    "iso_3166_2": "ISO 3166-2:NI",
    "region": "Americas",
    "sub_region": "Central America",
    "region_code": "019",
    "sub_region_code": "013"
  },
  {
    "name": "Niger",
    "alpha2": "NE",
    "alpha3": "NER",
    "country_code": "562",
    "iso_3166_2": "ISO 3166-2:NE",
    "region": "Africa",
    "sub_region": "Western Africa",
    "region_code": "002",
    "sub_region_code": "011"
  },
  {
    "name": "Nigeria",
    "alpha2": "NG",
    "alpha3": "NGA",
    "country_code": "566",
    "iso_3166_2": "ISO 3166-2:NG",
    "region": "Africa",
    "sub_region": "Western Africa",
    "region_code": "002",
    "sub_region_code": "011"
  },
  {
    "name": "Niue",
    "alpha2": "NU",
    "alpha3": "NIU",
    "country_code": "570",
    "iso_3166_2": "ISO 3166-2:NU",
    "region": "Oceania",
    "sub_region": "Polynesia",
    "region_code": "009",
    "sub_region_code": "061"
  },
  {
    "name": "Norfolk Island",
    "alpha2": "NF",
    "alpha3": "NFK",
    "country_code": "574",
    "iso_3166_2": "ISO 3166-2:NF",
    "region": "Oceania",
    "sub_region": "Australia and New Zealand",
    "region_code": "009",
    "sub_region_code": "053"
  },
  {
    "name": "Northern Mariana Islands",
    "alpha2": "MP",
    "alpha3": "MNP",
    "country_code": "580",
    "iso_3166_2": "ISO 3166-2:MP",
    "region": "Oceania",
    "sub_region": "Micronesia",
    "region_code": "009",
    "sub_region_code": "057"
  },
  {
    "name": "Norway",
    "alpha2": "NO",
    "alpha3": "NOR",
    "country_code": "578",
    "iso_3166_2": "ISO 3166-2:NO",
    "region": "Europe",
    "sub_region": "Northern Europe",
    "region_code": "150",
    "sub_region_code": "154"
  },
  {
    "name": "Oman",
    "alpha2": "OM",
    "alpha3": "OMN",
    "country_code": "512",
    "iso_3166_2": "ISO 3166-2:OM",
    "region": "Asia",
    "sub_region": "Western Asia",
    "region_code": "142",
    "sub_region_code": "145"
  },
  {
    "name": "Pakistan",
    "alpha2": "PK",
    "alpha3": "PAK",
    "country_code": "586",
    "iso_3166_2": "ISO 3166-2:PK",
    "region": "Asia",
    "sub_region": "Southern Asia",
    "region_code": "142",
    "sub_region_code": "034"
  },
  {
    "name": "Palau",
    "alpha2": "PW",
    "alpha3": "PLW",
    "country_code": "585",
    "iso_3166_2": "ISO 3166-2:PW",
    "region": "Oceania",
    "sub_region": "Micronesia",
    "region_code": "009",
    "sub_region_code": "057"
  },
  {
    "name": "Palestine, State of",
    "alpha2": "PS",
    "alpha3": "PSE",
    "country_code": "275",
    "iso_3166_2": "ISO 3166-2:PS",
    "region": "Asia",
    "sub_region": "Western Asia",
    "region_code": "142",
    "sub_region_code": "145"
  },
  {
    "name": "Panama",
    "alpha2": "PA",
    "alpha3": "PAN",
    "country_code": "591",
    "iso_3166_2": "ISO 3166-2:PA",
    "region": "Americas",
    "sub_region": "Central America",
    "region_code": "019",
    "sub_region_code": "013"
  },
  {
    "name": "Papua New Guinea",
    "alpha2": "PG",
    "alpha3": "PNG",
    "country_code": "598",
    "iso_3166_2": "ISO 3166-2:PG",
    "region": "Oceania",
    "sub_region": "Melanesia",
    "region_code": "009",
    "sub_region_code": "054"
  },
  {
    "name": "Paraguay",
    "alpha2": "PY",
    "alpha3": "PRY",
    "country_code": "600",
    "iso_3166_2": "ISO 3166-2:PY",
    "region": "Americas",
    "sub_region": "South America",
    "region_code": "019",
    "sub_region_code": "005"
  },
  {
    "name": "Peru",
    "alpha2": "PE",
    "alpha3": "PER",
    "country_code": "604",
    "iso_3166_2": "ISO 3166-2:PE",
    "region": "Americas",
    "sub_region": "South America",
    "region_code": "019",
    "sub_region_code": "005"
  },
  {
    "name": "Philippines",
    "alpha2": "PH",
    "alpha3": "PHL",
    "country_code": "608",
    "iso_3166_2": "ISO 3166-2:PH",
    "region": "Asia",
    "sub_region": "South-Eastern Asia",
    "region_code": "142",
    "sub_region_code": "035"
  },
  {
    "name": "Pitcairn",
    "alpha2": "PN",
    "alpha3": "PCN",
    "country_code": "612",
    "iso_3166_2": "ISO 3166-2:PN",
    "region": "Oceania",
    "sub_region": "Polynesia",
    "region_code": "009",
    "sub_region_code": "061"
  },
  {
    "name": "Poland",
    "alpha2": "PL",
    "alpha3": "POL",
    "country_code": "616",
    "iso_3166_2": "ISO 3166-2:PL",
    "region": "Europe",
    "sub_region": "Eastern Europe",
    "region_code": "150",
    "sub_region_code": "151"
  },
  {
    "name": "Portugal",
    "alpha2": "PT",
    "alpha3": "PRT",
    "country_code": "620",
    "iso_3166_2": "ISO 3166-2:PT",
    "region": "Europe",
    "sub_region": "Southern Europe",
    "region_code": "150",
    "sub_region_code": "039"
  },
  {
    "name": "Puerto Rico",
    "alpha2": "PR",
    "alpha3": "PRI",
    "country_code": "630",
    "iso_3166_2": "ISO 3166-2:PR",
    "region": "Americas",
    "sub_region": "Caribbean",
    "region_code": "019",
    "sub_region_code": "029"
  },
  {
    "name": "Qatar",
    "alpha2": "QA",
    "alpha3": "QAT",
    "country_code": "634",
    "iso_3166_2": "ISO 3166-2:QA",
    "region": "Asia",
    "sub_region": "Western Asia",
    "region_code": "142",
    "sub_region_code": "145"
  },
  {
    "name": "Réunion",
    "alpha2": "RE",
    "alpha3": "REU",
    "country_code": "638",
    "iso_3166_2": "ISO 3166-2:RE",
    "region": "Africa",
    "sub_region": "Eastern Africa",
    "region_code": "002",
    "sub_region_code": "014"
  },
  {
    "name": "Romania",
    "alpha2": "RO",
    "alpha3": "ROU",
    "country_code": "642",
    "iso_3166_2": "ISO 3166-2:RO",
    "region": "Europe",
    "sub_region": "Eastern Europe",
    "region_code": "150",
    "sub_region_code": "151"
  },
  {
    "name": "Russian Federation",
    "alpha2": "RU",
    "alpha3": "RUS",
    "country_code": "643",
    "iso_3166_2": "ISO 3166-2:RU",
    "region": "Europe",
    "sub_region": "Eastern Europe",
    "region_code": "150",
    "sub_region_code": "151"
  },
  {
    "name": "Rwanda",
    "alpha2": "RW",
    "alpha3": "RWA",
    "country_code": "646",
    "iso_3166_2": "ISO 3166-2:RW",
    "region": "Africa",
    "sub_region": "Eastern Africa",
    "region_code": "002",
    "sub_region_code": "014"
  },
  {
    "name": "Saint Barthélemy",
    "alpha2": "BL",
    "alpha3": "BLM",
    "country_code": "652",
    "iso_3166_2": "ISO 3166-2:BL",
    "region": "Americas",
    "sub_region": "Caribbean",
    "region_code": "019",
    "sub_region_code": "029"
  },
  {
    "name": "Saint Helena, Ascension and Tristan da Cunha",
    "alpha2": "SH",
    "alpha3": "SHN",
    "country_code": "654",
    "iso_3166_2": "ISO 3166-2:SH",
    "region": "Africa",
    "sub_region": "Western Africa",
    "region_code": "002",
    "sub_region_code": "011"
  },
  {
    "name": "Saint Kitts and Nevis",
    "alpha2": "KN",
    "alpha3": "KNA",
    "country_code": "659",
    "iso_3166_2": "ISO 3166-2:KN",
    "region": "Americas",
    "sub_region": "Caribbean",
    "region_code": "019",
    "sub_region_code": "029"
  },
  {
    "name": "Saint Lucia",
    "alpha2": "LC",
    "alpha3": "LCA",
    "country_code": "662",
    "iso_3166_2": "ISO 3166-2:LC",
    "region": "Americas",
    "sub_region": "Caribbean",
    "region_code": "019",
    "sub_region_code": "029"
  },
  {
    "name": "Saint Martin (French part)",
    "alpha2": "MF",
    "alpha3": "MAF",
    "country_code": "663",
    "iso_3166_2": "ISO 3166-2:MF",
    "region": "Americas",
    "sub_region": "Caribbean",
    "region_code": "019",
    "sub_region_code": "029"
  },
  {
    "name": "Saint Pierre and Miquelon",
    "alpha2": "PM",
    "alpha3": "SPM",
    "country_code": "666",
    "iso_3166_2": "ISO 3166-2:PM",
    "region": "Americas",
    "sub_region": "Northern America",
    "region_code": "019",
    "sub_region_code": "021"
  },
  {
    "name": "Saint Vincent and the Grenadines",
    "alpha2": "VC",
    "alpha3": "VCT",
    "country_code": "670",
    "iso_3166_2": "ISO 3166-2:VC",
    "region": "Americas",
    "sub_region": "Caribbean",
    "region_code": "019",
    "sub_region_code": "029"
  },
  {
    "name": "Samoa",
    "alpha2": "WS",
    "alpha3": "WSM",
    "country_code": "882",
    "iso_3166_2": "ISO 3166-2:WS",
    "region": "Oceania",
    "sub_region": "Polynesia",
    "region_code": "009",
    "sub_region_code": "061"
  },
  {
    "name": "San Marino",
    "alpha2": "SM",
    "alpha3": "SMR",
    "country_code": "674",
    "iso_3166_2": "ISO 3166-2:SM",
    "region": "Europe",
    "sub_region": "Southern Europe",
    "region_code": "150",
    "sub_region_code": "039"
  },
  {
    "name": "Sao Tome and Principe",
    "alpha2": "ST",
    "alpha3": "STP",
    "country_code": "678",
    "iso_3166_2": "ISO 3166-2:ST",
    "region": "Africa",
    "sub_region": "Middle Africa",
    "region_code": "002",
    "sub_region_code": "017"
  },
  {
    "name": "Saudi Arabia",
    "alpha2": "SA",
    "alpha3": "SAU",
    "country_code": "682",
    "iso_3166_2": "ISO 3166-2:SA",
    "region": "Asia",
    "sub_region": "Western Asia",
    "region_code": "142",
    "sub_region_code": "145"
  },
  {
    "name": "Senegal",
    "alpha2": "SN",
    "alpha3": "SEN",
    "country_code": "686",
    "iso_3166_2": "ISO 3166-2:SN",
    "region": "Africa",
    "sub_region": "Western Africa",
    "region_code": "002",
    "sub_region_code": "011"
  },
  {
    "name": "Serbia",
    "alpha2": "RS",
    "alpha3": "SRB",
    "country_code": "688",
    "iso_3166_2": "ISO 3166-2:RS",
    "region": "Europe",
    "sub_region": "Southern Europe",
    "region_code": "150",
    "sub_region_code": "039"
  },
  {
    "name": "Seychelles",
    "alpha2": "SC",
    "alpha3": "SYC",
    "country_code": "690",
    "iso_3166_2": "ISO 3166-2:SC",
    "region": "Africa",
    "sub_region": "Eastern Africa",
    "region_code": "002",
    "sub_region_code": "014"
  },
  {
    "name": "Sierra Leone",
    "alpha2": "SL",
    "alpha3": "SLE",
    "country_code": "694",
    "iso_3166_2": "ISO 3166-2:SL",
    "region": "Africa",
    "sub_region": "Western Africa",
    "region_code": "002",
    "sub_region_code": "011"
  },
  {
    "name": "Singapore",
    "alpha2": "SG",
    "alpha3": "SGP",
    "country_code": "702",
    "iso_3166_2": "ISO 3166-2:SG",
    "region": "Asia",
    "sub_region": "South-Eastern Asia",
    "region_code": "142",
    "sub_region_code": "035"
  },
  {
    "name": "Sint Maarten (Dutch part)",
    "alpha2": "SX",
    "alpha3": "SXM",
    "country_code": "534",
    "iso_3166_2": "ISO 3166-2:SX",
    "region": "Americas",
    "sub_region": "Caribbean",
    "region_code": "019",
    "sub_region_code": "029"
  },
  {
    "name": "Slovakia",
    "alpha2": "SK",
    "alpha3": "SVK",
    "country_code": "703",
    "iso_3166_2": "ISO 3166-2:SK",
    "region": "Europe",
    "sub_region": "Eastern Europe",
    "region_code": "150",
    "sub_region_code": "151"
  },
  {
    "name": "Slovenia",
    "alpha2": "SI",
    "alpha3": "SVN",
    "country_code": "705",
    "iso_3166_2": "ISO 3166-2:SI",
    "region": "Europe",
    "sub_region": "Southern Europe",
    "region_code": "150",
    "sub_region_code": "039"
  },
  {
    "name": "Solomon Islands",
    "alpha2": "SB",
    "alpha3": "SLB",
    "country_code": "090",
    "iso_3166_2": "ISO 3166-2:SB",
    "region": "Oceania",
    "sub_region": "Melanesia",
    "region_code": "009",
    "sub_region_code": "054"
  },
  {
    "name": "Somalia",
    "alpha2": "SO",
    "alpha3": "SOM",
    "country_code": "706",
    "iso_3166_2": "ISO 3166-2:SO",
    "region": "Africa",
    "sub_region": "Eastern Africa",
    "region_code": "002",
    "sub_region_code": "014"
  },
  {
    "name": "South Africa",
    "alpha2": "ZA",
    "alpha3": "ZAF",
    "country_code": "710",
    "iso_3166_2": "ISO 3166-2:ZA",
    "region": "Africa",
    "sub_region": "Southern Africa",
    "region_code": "002",
    "sub_region_code": "018"
  },
  {
    "name": "South Georgia and the South Sandwich Islands",
    "alpha2": "GS",
    "alpha3": "SGS",
    "country_code": "239",
    "iso_3166_2": "ISO 3166-2:GS",
    "sub_region_code": null,
    "region_code": null,
    "sub_region": null,
    "region": null
  },
  {
    "name": "South Sudan",
    "alpha2": "SS",
    "alpha3": "SSD",
    "country_code": "728",
    "iso_3166_2": "ISO 3166-2:SS",
    "region": "Africa",
    "sub_region": "Eastern Africa",
    "region_code": "002",
    "sub_region_code": "014"
  },
  {
    "name": "Spain",
    "alpha2": "ES",
    "alpha3": "ESP",
    "country_code": "724",
    "iso_3166_2": "ISO 3166-2:ES",
    "region": "Europe",
    "sub_region": "Southern Europe",
    "region_code": "150",
    "sub_region_code": "039"
  },
  {
    "name": "Sri Lanka",
    "alpha2": "LK",
    "alpha3": "LKA",
    "country_code": "144",
    "iso_3166_2": "ISO 3166-2:LK",
    "region": "Asia",
    "sub_region": "Southern Asia",
    "region_code": "142",
    "sub_region_code": "034"
  },
  {
    "name": "Sudan",
    "alpha2": "SD",
    "alpha3": "SDN",
    "country_code": "729",
    "iso_3166_2": "ISO 3166-2:SD",
    "region": "Africa",
    "sub_region": "Northern Africa",
    "region_code": "002",
    "sub_region_code": "015"
  },
  {
    "name": "Suriname",
    "alpha2": "SR",
    "alpha3": "SUR",
    "country_code": "740",
    "iso_3166_2": "ISO 3166-2:SR",
    "region": "Americas",
    "sub_region": "South America",
    "region_code": "019",
    "sub_region_code": "005"
  },
  {
    "name": "Svalbard and Jan Mayen",
    "alpha2": "SJ",
    "alpha3": "SJM",
    "country_code": "744",
    "iso_3166_2": "ISO 3166-2:SJ",
    "region": "Europe",
    "sub_region": "Northern Europe",
    "region_code": "150",
    "sub_region_code": "154"
  },
  {
    "name": "Swaziland",
    "alpha2": "SZ",
    "alpha3": "SWZ",
    "country_code": "748",
    "iso_3166_2": "ISO 3166-2:SZ",
    "region": "Africa",
    "sub_region": "Southern Africa",
    "region_code": "002",
    "sub_region_code": "018"
  },
  {
    "name": "Sweden",
    "alpha2": "SE",
    "alpha3": "SWE",
    "country_code": "752",
    "iso_3166_2": "ISO 3166-2:SE",
    "region": "Europe",
    "sub_region": "Northern Europe",
    "region_code": "150",
    "sub_region_code": "154"
  },
  {
    "name": "Switzerland",
    "alpha2": "CH",
    "alpha3": "CHE",
    "country_code": "756",
    "iso_3166_2": "ISO 3166-2:CH",
    "region": "Europe",
    "sub_region": "Western Europe",
    "region_code": "150",
    "sub_region_code": "155"
  },
  {
    "name": "Syrian Arab Republic",
    "alpha2": "SY",
    "alpha3": "SYR",
    "country_code": "760",
    "iso_3166_2": "ISO 3166-2:SY",
    "region": "Asia",
    "sub_region": "Western Asia",
    "region_code": "142",
    "sub_region_code": "145"
  },
  {
    "name": "Taiwan, Province of China",
    "alpha2": "TW",
    "alpha3": "TWN",
    "country_code": "158",
    "iso_3166_2": "ISO 3166-2:TW",
    "region": "Asia",
    "sub_region": "Eastern Asia",
    "region_code": "142",
    "sub_region_code": "030"
  },
  {
    "name": "Tajikistan",
    "alpha2": "TJ",
    "alpha3": "TJK",
    "country_code": "762",
    "iso_3166_2": "ISO 3166-2:TJ",
    "region": "Asia",
    "sub_region": "Central Asia",
    "region_code": "142",
    "sub_region_code": "143"
  },
  {
    "name": "Tanzania, United Republic of",
    "alpha2": "TZ",
    "alpha3": "TZA",
    "country_code": "834",
    "iso_3166_2": "ISO 3166-2:TZ",
    "region": "Africa",
    "sub_region": "Eastern Africa",
    "region_code": "002",
    "sub_region_code": "014"
  },
  {
    "name": "Thailand",
    "alpha2": "TH",
    "alpha3": "THA",
    "country_code": "764",
    "iso_3166_2": "ISO 3166-2:TH",
    "region": "Asia",
    "sub_region": "South-Eastern Asia",
    "region_code": "142",
    "sub_region_code": "035"
  },
  {
    "name": "Timor-Leste",
    "alpha2": "TL",
    "alpha3": "TLS",
    "country_code": "626",
    "iso_3166_2": "ISO 3166-2:TL",
    "region": "Asia",
    "sub_region": "South-Eastern Asia",
    "region_code": "142",
    "sub_region_code": "035"
  },
  {
    "name": "Togo",
    "alpha2": "TG",
    "alpha3": "TGO",
    "country_code": "768",
    "iso_3166_2": "ISO 3166-2:TG",
    "region": "Africa",
    "sub_region": "Western Africa",
    "region_code": "002",
    "sub_region_code": "011"
  },
  {
    "name": "Tokelau",
    "alpha2": "TK",
    "alpha3": "TKL",
    "country_code": "772",
    "iso_3166_2": "ISO 3166-2:TK",
    "region": "Oceania",
    "sub_region": "Polynesia",
    "region_code": "009",
    "sub_region_code": "061"
  },
  {
    "name": "Tonga",
    "alpha2": "TO",
    "alpha3": "TON",
    "country_code": "776",
    "iso_3166_2": "ISO 3166-2:TO",
    "region": "Oceania",
    "sub_region": "Polynesia",
    "region_code": "009",
    "sub_region_code": "061"
  },
  {
    "name": "Trinidad and Tobago",
    "alpha2": "TT",
    "alpha3": "TTO",
    "country_code": "780",
    "iso_3166_2": "ISO 3166-2:TT",
    "region": "Americas",
    "sub_region": "Caribbean",
    "region_code": "019",
    "sub_region_code": "029"
  },
  {
    "name": "Tunisia",
    "alpha2": "TN",
    "alpha3": "TUN",
    "country_code": "788",
    "iso_3166_2": "ISO 3166-2:TN",
    "region": "Africa",
    "sub_region": "Northern Africa",
    "region_code": "002",
    "sub_region_code": "015"
  },
  {
    "name": "Turkey",
    "alpha2": "TR",
    "alpha3": "TUR",
    "country_code": "792",
    "iso_3166_2": "ISO 3166-2:TR",
    "region": "Asia",
    "sub_region": "Western Asia",
    "region_code": "142",
    "sub_region_code": "145"
  },
  {
    "name": "Turkmenistan",
    "alpha2": "TM",
    "alpha3": "TKM",
    "country_code": "795",
    "iso_3166_2": "ISO 3166-2:TM",
    "region": "Asia",
    "sub_region": "Central Asia",
    "region_code": "142",
    "sub_region_code": "143"
  },
  {
    "name": "Turks and Caicos Islands",
    "alpha2": "TC",
    "alpha3": "TCA",
    "country_code": "796",
    "iso_3166_2": "ISO 3166-2:TC",
    "region": "Americas",
    "sub_region": "Caribbean",
    "region_code": "019",
    "sub_region_code": "029"
  },
  {
    "name": "Tuvalu",
    "alpha2": "TV",
    "alpha3": "TUV",
    "country_code": "798",
    "iso_3166_2": "ISO 3166-2:TV",
    "region": "Oceania",
    "sub_region": "Polynesia",
    "region_code": "009",
    "sub_region_code": "061"
  },
  {
    "name": "Uganda",
    "alpha2": "UG",
    "alpha3": "UGA",
    "country_code": "800",
    "iso_3166_2": "ISO 3166-2:UG",
    "region": "Africa",
    "sub_region": "Eastern Africa",
    "region_code": "002",
    "sub_region_code": "014"
  },
  {
    "name": "Ukraine",
    "alpha2": "UA",
    "alpha3": "UKR",
    "country_code": "804",
    "iso_3166_2": "ISO 3166-2:UA",
    "region": "Europe",
    "sub_region": "Eastern Europe",
    "region_code": "150",
    "sub_region_code": "151"
  },
  {
    "name": "United Arab Emirates",
    "alpha2": "AE",
    "alpha3": "ARE",
    "country_code": "784",
    "iso_3166_2": "ISO 3166-2:AE",
    "region": "Asia",
    "sub_region": "Western Asia",
    "region_code": "142",
    "sub_region_code": "145"
  },
  {
    "name": "United Kingdom of Great Britain and Northern Ireland",
    "alpha2": "GB",
    "alpha3": "GBR",
    "country_code": "826",
    "iso_3166_2": "ISO 3166-2:GB",
    "region": "Europe",
    "sub_region": "Northern Europe",
    "region_code": "150",
    "sub_region_code": "154"
  },
  {
    "name": "United States of America",
    "alpha2": "US",
    "alpha3": "USA",
    "country_code": "840",
    "iso_3166_2": "ISO 3166-2:US",
    "region": "Americas",
    "sub_region": "Northern America",
    "region_code": "019",
    "sub_region_code": "021"
  },
  {
    "name": "United States Minor Outlying Islands",
    "alpha2": "UM",
    "alpha3": "UMI",
    "country_code": "581",
    "iso_3166_2": "ISO 3166-2:UM",
    "sub_region_code": null,
    "region_code": null,
    "sub_region": null,
    "region": null
  },
  {
    "name": "Uruguay",
    "alpha2": "UY",
    "alpha3": "URY",
    "country_code": "858",
    "iso_3166_2": "ISO 3166-2:UY",
    "region": "Americas",
    "sub_region": "South America",
    "region_code": "019",
    "sub_region_code": "005"
  },
  {
    "name": "Uzbekistan",
    "alpha2": "UZ",
    "alpha3": "UZB",
    "country_code": "860",
    "iso_3166_2": "ISO 3166-2:UZ",
    "region": "Asia",
    "sub_region": "Central Asia",
    "region_code": "142",
    "sub_region_code": "143"
  },
  {
    "name": "Vanuatu",
    "alpha2": "VU",
    "alpha3": "VUT",
    "country_code": "548",
    "iso_3166_2": "ISO 3166-2:VU",
    "region": "Oceania",
    "sub_region": "Melanesia",
    "region_code": "009",
    "sub_region_code": "054"
  },
  {
    "name": "Venezuela (Bolivarian Republic of)",
    "alpha2": "VE",
    "alpha3": "VEN",
    "country_code": "862",
    "iso_3166_2": "ISO 3166-2:VE",
    "region": "Americas",
    "sub_region": "South America",
    "region_code": "019",
    "sub_region_code": "005"
  },
  {
    "name": "Viet Nam",
    "alpha2": "VN",
    "alpha3": "VNM",
    "country_code": "704",
    "iso_3166_2": "ISO 3166-2:VN",
    "region": "Asia",
    "sub_region": "South-Eastern Asia",
    "region_code": "142",
    "sub_region_code": "035"
  },
  {
    "name": "Virgin Islands (British)",
    "alpha2": "VG",
    "alpha3": "VGB",
    "country_code": "092",
    "iso_3166_2": "ISO 3166-2:VG",
    "region": "Americas",
    "sub_region": "Caribbean",
    "region_code": "019",
    "sub_region_code": "029"
  },
  {
    "name": "Virgin Islands (U.S.)",
    "alpha2": "VI",
    "alpha3": "VIR",
    "country_code": "850",
    "iso_3166_2": "ISO 3166-2:VI",
    "region": "Americas",
    "sub_region": "Caribbean",
    "region_code": "019",
    "sub_region_code": "029"
  },
  {
    "name": "Wallis and Futuna",
    "alpha2": "WF",
    "alpha3": "WLF",
    "country_code": "876",
    "iso_3166_2": "ISO 3166-2:WF",
    "region": "Oceania",
    "sub_region": "Polynesia",
    "region_code": "009",
    "sub_region_code": "061"
  },
  {
    "name": "Western Sahara",
    "alpha2": "EH",
    "alpha3": "ESH",
    "country_code": "732",
    "iso_3166_2": "ISO 3166-2:EH",
    "region": "Africa",
    "sub_region": "Northern Africa",
    "region_code": "002",
    "sub_region_code": "015"
  },
  {
    "name": "Yemen",
    "alpha2": "YE",
    "alpha3": "YEM",
    "country_code": "887",
    "iso_3166_2": "ISO 3166-2:YE",
    "region": "Asia",
    "sub_region": "Western Asia",
    "region_code": "142",
    "sub_region_code": "145"
  },
  {
    "name": "Zambia",
    "alpha2": "ZM",
    "alpha3": "ZMB",
    "country_code": "894",
    "iso_3166_2": "ISO 3166-2:ZM",
    "region": "Africa",
    "sub_region": "Eastern Africa",
    "region_code": "002",
    "sub_region_code": "014"
  },
  {
    "name": "Zimbabwe",
    "alpha2": "ZW",
    "alpha3": "ZWE",
    "country_code": "716",
    "iso_3166_2": "ISO 3166-2:ZW",
    "region": "Africa",
    "sub_region": "Eastern Africa",
    "region_code": "002",
    "sub_region_code": "014"
  }
]

export default {
  getCountries() {
    return _.orderBy(countries, ['name', 'alpha2'], ['asc', 'asc']);
  },
  getStates() {
    return _.orderBy(states, ['id', 'name'], ['asc', 'asc']);
  }
}
