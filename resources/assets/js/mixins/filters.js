import _ from 'lodash';
import accounting from 'accounting';
const orderBy = function(items, field) {
  return _.orderBy(items, [item => item[field].toString().trim().toLowerCase()]);
};

const capitalize = require('capitalize');

const cap = (string) => {
  if (_.isNull(string)) {
    return null;
  }
  return capitalize(string);
};

const capWords = (string) => {
  if (_.isNull(string)) {
    return null;
  }
  return capitalize.words(string);
}

export default {
  methods: {
    pluralize: require('pluralize'),
    //accounting: accounting, // This causes vue > 2.5.13 to bomb out hard, so it needs to be removed.
    formatMoney: accounting.formatMoney,
    formatNumber: accounting.formatNumber,
    capitalize: cap,
    capitalizeWords: capWords,
    orderBy: orderBy,
  }
}
