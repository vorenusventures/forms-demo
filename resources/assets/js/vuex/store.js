import Vue from "vue";
import Vuex from "vuex";
import logger from "vuex/dist/logger";
import * as ACTIONS from "./actions";
import * as GETTERS from "./getters";
import _ from 'lodash';

Vue.use(Vuex);

const state = {
  leads: [],
};

const mutations = {
  SET_LEAD(state, lead) {
    const index = _.findIndex(state.leads, l => l.id === lead.id);
    if (index === -1) {
      state.leads.push(lead);
    } else {
      state.leads[index] = lead;
    }
  },
  SET_LEADS(state, leads) {
    state.leads = leads;
  }
};

const debug = process.env.NODE_ENV !== 'production';

// See https://github.com/vuejs/vuex/blob/dev/examples/shopping-cart/store/index.js
export default new Vuex.Store({
  actions: ACTIONS,
  getters: GETTERS,
  mutations,
  state,
  strict: debug,
  plugins: debug ? [logger] : []
});
