window._ = require('lodash');

/**
 * We'll load jQuery and the Bootstrap jQuery plugin which provides support
 * for JavaScript based Bootstrap features such as modals and tabs. This
 * code may be modified to fit the specific needs of your application.
 */

try {
  window.$ = window.jQuery = require('jquery');

  require('bootstrap-sass');
} catch (e) {}

/**
 * We'll load the axios HTTP library which allows us to easily issue requests
 * to our Laravel back-end. This library automatically handles sending the
 * CSRF token as a header based on the value of the "XSRF" token cookie.
 */

window.axios = require('axios');

import toastr from 'toastr';
window.toastr = toastr;
toastr.options.preventDuplicates = true;
toastr.options.closeButton = true;
toastr.options.timeOut = 2000;
toastr.options.extendedTimeOut = 2000;
toastr.options.progressBar = true;
toastr.options.positionClass = 'toast-top-full-width';

import mobilelocker from './mobilelocker-tracking';
window.mobilelocker = mobilelocker;

require('./filters/filters');
require('./forms/SparkErrors');
require('./forms/SparkForm');
