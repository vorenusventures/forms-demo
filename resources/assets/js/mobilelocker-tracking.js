import axios from 'axios';
import {isNull, isObject, isString, merge} from 'lodash';
import toastr from 'toastr';
import platform from 'platform';
import URI from 'urijs';
import {v4 as uuid} from 'uuid';

const logger = {
  log: console.log,
  info: toastr.info,
  error: toastr.error,
  warn: toastr.warn
};

const NOTIFY_NONE = 0; // Never notify me when the link is clicked.
const NOTIFY_FIRST = 1; // Notify me the first time the link is clicked.
const NOTIFY_EVERY = 2; // Notify me every time the link is clicked.
const NOTIFY_WEEKLY = 3; // Include the click stats in my weekly notification.
const NOTIFY_MONTHLY = 4; // Include the click stats in my monthly notification.

function getTokenFromURL() {
  const uri = URI(window.location.href);
  const params = uri.search(true);
  if (isObject(params) && params.jwt) {
    return params.jwt;
  }
  return null;
}

/**
 * @see https://jrxcodes.com/how-to-decode-jwt-in-javascript
 * @param token
 * @returns {null|array}
 */
function parseToken(token) {
  if (isNull(token)) {
    return null;
  }
  if (!isString(token)) {
    logger.error('JWT is not a string');
    return null;
  }
  try {
    return JSON.parse(atob(token.split('.')[1]));
  } catch (e) {
    return null;
  }
}

const token = getTokenFromURL();
const jwt = parseToken(token);
const userID = (jwt && jwt.user_id) ? jwt.user_id : null;
console.log('jwt', jwt);
console.log('userID', userID);

/**
 * Return TRUE if running in the iOS or desktop app.
 * @returns {boolean}
 */
function isMobileLockerApp() {
  return (typeof ML_ENVIRONMENT !== 'undefined' || navigator.userAgent.indexOf('Locker') !== -1);
}

/**
 * Return TRUE when running in the browser version of Mobile Locker on the secure CDN
 * MLD-2254
 * @returns {boolean}
 */
function isMobileLockerCDN() {
  return isObject(window) &&
    isObject(window.location)
    && isString(window.location.hostname)
    && (window.location.hostname.endsWith('.app.mobilelocker.com')
      || window.location.hostname.endsWith('.eu.mobilelocker.com') // MLD-4690
      || window.location.hostname.endsWith('.staging.mobilelocker.com')
      || window.location.hostname.endsWith('.dev.mobilelocker.com'));
}

/**
 * Return TRUE if running in the browser, iOS, or desktop app.
 * MLD-2254
 * @returns {*|(function(): string)|string|boolean}
 */
function isMobileLocker() {
  return isMobileLockerApp() || isMobileLockerCDN();
}

function getEndpoint(uri = '') {
  if (isMobileLockerCDN() && isObject(jwt) && isString(jwt.base_url)) {
    return `${jwt.base_url}/api/browser${uri}`
  }
  return `/mobilelocker/api${uri}`;
}

function getBaseEndpoint() {
  if (isMobileLockerCDN() && isObject(jwt) && isString(jwt.base_url)) {
    return `${jwt.base_url}/api/browser`
  }

  return '';
}

let instance = axios.create({
  baseURL: getBaseEndpoint(),
  timeout: 10000,
})

instance.interceptors.request.use(config => {
  if (isObject(jwt) && isObject(jwt.device)) {
    const device = jwt.device;
    config.headers.common['Authorization'] = `Bearer ${token}`;
    config.headers.common['X-ML-TOKEN'] = token;
    config.headers.common['X-ML-DEVICE-UUID'] = device.uuid;
    config.headers.common['X-ML-DEVICE-NAME'] = device.uuid;
    config.headers.common['X-ML-DEVICE-SYSTEM-NAME'] = platform.os.family;
    config.headers.common['X-ML-DEVICE-SYSTEM-VERSION'] = platform.os.version;
    config.headers.common['X-ML-DEVICE-MODEL'] = 'browser';
    config.headers.common['X-ML-DEVICE-LOCAL-MODEL'] = 'browser';
    config.headers.common['X-ML-DEVICE-APP-NAME'] = 'app.mobilelocker.com';
    config.headers.common['X-ML-DEVICE-APP-VERSION'] = 0;
    config.headers.common['X-ML-DEVICE-APP-BUILD'] = 0;
    if (isObject(jwt.payload) && isString(jwt.payload.session_uuid)) {
      config.headers.common['X-ML-SESSION-UUID'] = jwt.payload.session_uuid;
    }
  }
  return config;
});

/**
 * Trigger the app to refresh the WKWebView.
 * @since 3.0.2
 * @returns {AxiosPromise<any>}
 */
function reloadPresentation() {
  return instance.post(getEndpoint('/presentation/reload'));
}

/**
 * Show the Menu in the presentation.  For now, it will disappear after 3 seconds.
 * @since 3.0.2
 * @returns {AxiosPromise}
 */
function showMenu() {
  return instance.post(getEndpoint('/menu/show'));
}

/**
 * Show the Comments screen in the presentation.
 * @since 3.1.0
 * @returns {AxiosPromise}
 */
function showComments() {
  return instance.post(getEndpoint('/comments/show'));
}

/**
 * Add a Comment to the presentation
 * @since 3.1.0
 * @param body - the text of the comment
 * @param path - the path or URL you are currently at in the presentation
 * @returns {Promise<R>|Promise.<TResult>|Promise<R2|R1>}
 */
function addComment(body, path) {
  return logEvent('comment', 'post', path, {body}, 'addComment');
}

/**
 * Retrieve the list of Comments as JSON for the presentation
 * @since 3.1.0
 * @returns {Promise<R>|Promise.<TResult>|Promise<R2|R1>}
 */
function getPresentationComments() {
  return instance.get(getEndpoint('/presentation/comments'))
    .then(response => {
      return response.data;
    })
}

/**
 * Retrieve the Account objects from CRM.
 * MLI-198
 * @returns {Promise<AxiosResponse<any>>}
 */
function getAccounts() {
  return instance.get(getEndpoint('/crm/accounts'))
    .then(response => {
      return response.data;
    })
}

/**
 * MLI-301
 * Retrieve the Address objects from CRM.
 * This is only relevant to Veeva CRM right now.
 * @returns {Promise<AxiosResponse<any>>}
 */
function getAddresses() {
  return instance.get(getEndpoint('/crm/addresses'))
    .then(response => response.data);
}

/**
 * Retrieve the Contact objects from CRM.
 * @returns {Promise<AxiosResponse<any>>}
 */
function getContacts() {
  return instance.get(getEndpoint('/crm/contacts'))
    .then(response => {
      return response.data;
    })
}

/**
 * MLI-305 Retrieve the User objects from CRM.
 * This requires iOS app version 3.18.8 or later.
 * @returns {Promise<AxiosResponse<any>>}
 */
function getCRMUsers() {
  return instance.get(getEndpoint('/crm/users'))
    .then(response => {
      return response.data;
    })
}

/**
 * Return a JSON object representing the current Mobile Locker user.
 * @since 3.0.1
 * @returns {Promise.<TResult>|Promise<R2|R1>|Promise<R>}
 */
function getUser() {
  return instance.get(getEndpoint('/user'))
    .then(response => {
      return response.data;
    });
}

/**
 * Return a JSON object representing the current presentation.
 * @since 3.0.1
 * @returns {Promise.<TResult>|Promise<R2|R1>|Promise<R>}
 */
function getPresentation() {
  return instance.get(getEndpoint('/presentation'))
    .then(response => {
      return response.data;
    });
}

/**
 * Return a JSON array of the presentations that the user has access to.
 * @since 3.0.1
 * @returns {Promise.<TResult>|Promise<R>|Promise<R2|R1>}
 */
function getPresentations() {
  return instance.get(getEndpoint('/presentations'))
    .then(response => {
      return response.data;
    });
}

/**
 * Return a JSON array containing all events that have been recorded for this presentation, in all sessions.
 * @since 3.0.1
 * @returns {Promise.<TResult>|Promise<R>|Promise<R2|R1>}
 */
function getPresentationEvents() {
  return instance.get(getEndpoint('/presentation/events'))
    .then(response => {
      return response.data;
    });
}

/**
 * Return a JSON array containing all events that have been recorded for this presentation in the current session.
 * @since 3.0.1
 * @returns {Promise.<TResult>|Promise<R>|Promise<R2|R1>}
 */
function getSessionEvents() {
  return instance.get(getEndpoint('/session/events'))
    .then(response => {
      return response.data;
    });
}

/**
 * Open the Barcode/QR Code Scanner.
 * This only works in the iOS app.
 * @since 3.0.4
 * @returns {Promise<AxiosResponse<any>>}
 */
function openScanner() {
  return instance.post(getEndpoint('/open-scanner'))
    .then(response => {
      return response.data;
    })
}

/**
 * MLI-198
 * @returns {Promise<*>}
 */
function getBusinessCards() {
  const endpoint = getEndpoint('/cards');
  console.log('ENDPOINT', endpoint);
  return instance.get(endpoint)
    .then(response => {
      return response.data;
    })
}

/**
 * Save the response from a FormBuilder Form
 * @since 3.0.49
 * @param formResponse
 */
function saveFormResponse(formResponse) {
  return this.submitForm(formResponse.form_uuid, formResponse);
}

/**
 * @since 2.15
 * @param formName
 * @param formInput
 * @returns {Promise}
 */
function submitForm(formName, formInput) {
  if (isMobileLocker()) {
    return logEvent('data-capture', formName, formName, formInput, 'capturedata');
  }
  return new Promise((resolve, reject) => {
    logger.info('Faking form submission');
    setTimeout(function () {
      resolve(true);
    }, 3000);
  });
}

/**
 * Return TRUE if the client is online
 * @since 2.15
 * @returns {boolean}
 */
function online() {
  return window.navigator.onLine;
}

/**
 * Send an email to a recipient
 * @param {string|object} to - Required - The recipient's email address or an object with "name" and "email" properties
 * @param {string} subject - Required - The subject line
 * @param {string|null} body optional - The HTML body of the email.
 * @param {string|null} attachment - optional attachment path, relative to the root of the presentation (no slash prefix).
 * @param {string|null} template - optional path for the HTML template, relative to the root of the presentation (no slash prefix). If not provided, the default Mobile Locker template will be used.
 * @param {object|null} formData - optional object with additional data
 * @returns {Promise}
 */
function sendEmail(to, subject, body = null, attachment = null, template = null, formData = null) {
  if (!to) {
    return Promise.reject('Invalid recipient');
  }
  const recipient = {
    name: null,
    email: null,
  };
  if (typeof to === 'string') {
    recipient.email = to;
  } else if (typeof to === 'object') {
    recipient.name = to.name;
    recipient.email = to.email;
  }

  const form = {
    email: {
      recipient,
      subject,
      body,
      attachment,
      template,
    },
    formData,
  };
  return submitForm('email_form', form);
}

/**
 * MLD-948
 * @param recipients - each entry in this array must be an object with the "email" attribute set to the recipient's email address. The "name" attribute is optional
 * @param notificationLevel
 * @param sendReminders - If true, reminder emails will be sent to the recipient every day for seven days, until they click the link.
 * @return Promise
 */
function sharePresentation(recipients = [], notificationLevel = NOTIFY_EVERY, sendReminders = true) {
  return new Promise((resolve, reject) => {
    if (isMobileLocker()) {
      const data = {
        notification_level: notificationLevel,
        recipients: recipients,
        send_reminders: sendReminders,
        source: 'app',
      };

      return logEvent('share', 'share-presentation', '/share', data, 'capturedata');
    } else {
      logger.info('Faking share presentation event');
      setTimeout(function () {
        resolve(true);
      }, 3000);
    }
  });
}


/**
 * Open the iOS native Compose Email window
 * @since 3.0.4
 * @param recipient - optional, the email address of the recipient
 * @param subject - the subject line of the email
 * @param body - the body of the email (it can be HTML code)
 * @param isHTML - boolean TRUE if the body is HTML, false otherwise
 * @param attachmentPath - optional, path to the file in the presentation to attach to the email
 * @returns {*}
 */
function composeEmail(recipient, subject, body, isHTML, attachmentPath) {
  if (isMobileLocker()) {
    return instance.post(getEndpoint('/compose-email'), {
      recipient: recipient,
      subject: subject,
      body: body,
      isHTML: isHTML,
      attachment: attachmentPath
    })
      .then(response => {
        return response.data;
      })
  } else {
    return false;
  }
}

/**
 * @since 2.15
 * @param pdfPath
 * @param title
 * @param customOptions
 * @returns {AxiosPromise<any>|Promise<boolean>}
 */
function openPDF(pdfPath, title, customOptions) {
  if (isMobileLocker()) {
    const options = {
      'search': 'Y', //Y or N.
      'outline': 'Y', //Y or N
      'annotations': 'Y', //Y or N
      'vertical': 'N', //Y or N
      'toolbar': 'Y', //Y or N.  If N, the toolbar will not display (we highly recommend keeping this as Y)
      'scrolling': 'Y', //Y or N
      'maximumZoomScale': 5.0, //0.0 to 10.0
      'pageModeDouble': 'N', //Y or N: if Y, show 2 pages next to each other
      'statusBarStyleSetting': 5, //1, 2, 3, 4, 5
      'fitWidth': 'N', //Y or N - if Y, pages are fit to screen width, not to either height or width (which one is larger – usually height.) Defaults to NO. fitWidth is currently not supported for vertical scrolling. This is a know limitation of the PDF Rendering Library, not Mobile Locker.
      'viewMode': 1//1=Document, 2=Thumbnails
    };
    merge(options, customOptions);

    const data = {
      filename: pdfPath,
      title: title
    };
    return logEvent('PDF', 'Open', pdfPath, data, 'showpdf');
  } else {
    window.open(pdfPath, '_blank');
  }
}

/**
 * Put the session into "PRACTICE" mode so its events will not be counted in reports.
 * MLD-475
 * @param uri
 * @since 3.0.47
 */
function liveMode(uri = '') {
  logEvent('session_live_mode', 'activate', uri, null);
}

/**
 * Put the session into "PRACTICE" mode so its events will not be counted in reports.
 * MLD-475
 * @param uri
 * @since 3.0.47
 */
function practiceMode(uri = '') {
  logEvent('session_live_mode', 'deactivate', uri, null);
}

/**
 *
 * @param category - string
 * @param action - string
 * @param uri - string
 * @param data - optional object
 * @param method - string
 */
function logEvent(category, action, uri, data, method = 'trackevent') {
  if (isMobileLocker()) {
    const now = Date.now();
    const event = {
      method: method,
      category: category,
      action: action,
      uri: uri,
      path: uri,
      data: data,
    }
    if (isMobileLockerApp()) {
      // Running in the app
      return instance.post(getEndpoint(), event)
    } else if (userID) {
      // Running in the browser and it's a user, so log it as a DeviceEvent
      event.id = uuid();
      event.session_id = jwt.payload.session_uuid;
      event.session_started_at = jwt.payload.session_started_at;
      event.event_at = Math.floor(now / 1000);
      event.event_at_ms = now;
      return instance.post(getEndpoint(`/device-events`), event)
    } else {
      // Running in the browser and it's a visitor, so log it as an Event
      event.event_at_ms = now;
      return instance.post(getEndpoint('/events'), event);
    }
  }
  return Promise.resolve(true);
}

/**
 * @deprecated
 * @param iframeURL
 */
function iframe(iframeURL) {
  let iframe = document.createElement('IFRAME');
  iframe.setAttribute('src', iframeURL);
  iframe.setAttribute('style', 'display: none;');
  document.documentElement.appendChild(iframe);
  setTimeout(function () {
    iframe.parentNode.removeChild(iframe);
    iframe = null;
  }, 500);
}

function closePresentation() {
  if (isMobileLocker()) {
    return logEvent('presentation', 'close', 'close-presentation', null, 'close-presentation');
  }
  return Promise.resolve(false);
}

export default {
  isMobileLocker,
  isMobileLockerCDN, // MLD-2254
  isMobileLockerApp, // MLD-2254
  captureData: submitForm,
  submitForm,
  sharePresentation, // MLD-948
  saveFormResponse,
  sendEmail,
  openPDF,
  liveMode, // MLD-475
  practiceMode, // MLD-475
  logEvent,
  closePresentation,
  composeEmail,
  getUser,
  getPresentation,
  getPresentations,
  getSessionEvents,
  getPresentationEvents,
  openScanner,
  getBusinessCards, // MLI-198
  reloadPresentation,
  showMenu,
  showComments,
  getPresentationComments,
  addComment,
  getAccounts, // MLI-198
  getAddresses, // MLI-301
  getContacts, // MLI-198
  getCRMUsers, // MLI-305
  notificationLevels: {
    NOTIFY_NONE,
    NOTIFY_FIRST,
    NOTIFY_EVERY,
    NOTIFY_WEEKLY,
    NOTIFY_MONTHLY,
  },
};
