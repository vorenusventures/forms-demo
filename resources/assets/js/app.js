
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

import Vue from 'vue';
window.Vue = Vue;

import VueRouter from 'vue-router';
Vue.use(VueRouter);
import routes from './routes';
import Vuelidate from 'vuelidate';
Vue.use(Vuelidate);
import store from "./vuex/store";

const router = new VueRouter({
  linkActiveClass: 'active',
  routes,
});

const bus = new Vue();
window.bus = bus;

const app = new Vue({
  el: '#app',
  components: {
    'app': require('./components/app.vue').default,
  },
  router,
  store,
});
window.app = app;
