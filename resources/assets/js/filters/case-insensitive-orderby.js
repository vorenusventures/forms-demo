// https://mattstauffer.co/blog/case-insensitive-orderby-filter-with-vuejs
import Vue from 'vue';

Vue.filter('caseInsensitiveOrderBy', function (arr, sortKey, reverse) {
  // arr = convertArray(arr)
  if (!sortKey) {
    return arr
  }
  var order = (reverse && reverse < 0) ? -1 : 1
  // sort on a copy to avoid mutating original array
  return arr.slice().sort(function (a, b) {
    if (sortKey !== '$key') {
      if (Vue.util.isObject(a) && '$value' in a) a = a.$value
      if (Vue.util.isObject(b) && '$value' in b) b = b.$value
    }
    a = Vue.util.isObject(a) ? Vue.parsers.path.getPath(a, sortKey) : a
    b = Vue.util.isObject(b) ? Vue.parsers.path.getPath(b, sortKey) : b
    /*console.log(`${a} is of type ${typeof a}`);
    console.log(`${b} is of type ${typeof b}`);*/

    if (typeof a == 'string') {
      a = a.toLowerCase().trim();
      b = b.toLowerCase().trim();
      //console.log(`${a} is lowercased and ${b} is too.`);
    }

    return a === b ? 0 : a > b ? order : -order
  })
});
