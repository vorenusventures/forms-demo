import Vue from 'vue';
import moment from 'moment';
window.pluralize = require('pluralize');
window.accounting = require('accounting');

require ('./case-insensitive-orderby');
/**
 * Format the given date.
 */
Vue.filter('date', (value, format = 'YYYY-MM-DD', utc = true) => {
  if (utc) {
    return moment.utc(value).local().format(format)
  }
  return moment(value).format(format)
});

/**
 * Format the given date as a timestamp.
 */
Vue.filter('datetime', (value, format = 'YYYY-MM-DD h:mm:ss a', utc = true) => {
  if (utc) {
    return moment.utc(value).local().format(format);
  } else {
    return moment(value).format(format);
  }
});


/**
 * Format the given date into a relative time.
 */
Vue.filter('relative', value => {
  moment.updateLocale('en', {
    relativeTime : {
      future: "in %s",
      past:   "%s",
      s:  "1s",
      m:  "1m",
      mm: "%dm",
      h:  "1h",
      hh: "%dh",
      d:  "1d",
      dd: "%d days",
      M:  "1 month ago",
      MM: "%d months ago",
      y:  "1y",
      yy: "%dy"
    }
  });

  return moment.utc(value).local().fromNow();
});
