const mix = require('laravel-mix');
mix.disableSuccessNotifications();
mix.setPublicPath('public');
//mix.setResourceRoot('resource/locators');

// https://github.com/JeffreyWay/laravel-mix/blob/master/docs/autoloading.md
mix.autoload({
  axios: ['axios', 'window.axios'],
  jquery: ['$', 'jQuery', 'window.jQuery'],
  moment: ['moment', 'window.moment'],
});

mix.copyDirectory('node_modules/bootstrap-sass/assets/fonts/bootstrap', 'public/fonts');
mix.copyDirectory('node_modules/font-awesome/fonts', 'public/fonts');
mix.copyDirectory('resources/assets/images', 'public/images');
mix.copyDirectory('resources/assets/pdf', 'public/pdf');
mix.copy('resources/assets/js/forms.json', 'public/');
mix.copy('resources/views/index.html', 'public/');

mix.js('resources/assets/js/app.js', 'public/js/');
mix.sass('resources/assets/sass/app.scss', 'public/css/')
  .options({
    processCssUrls: false,
  });

mix.browserSync({
  notify: false, // Don't show notifications when build succeeds.
  open: false, // Don't open the browser when webpack starts.
  proxy: null, // has to be set to null for 'server' to work.
  server: { // https://browsersync.io/docs/options#option-server
    baseDir: 'public', // the base directory to serve
    directory: false, // no directory listing
    index: '/index.html',
  },
});
