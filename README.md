# Project setup

Install [Yarn](https://yarnpkg.com/en/docs/install) if you don't already have it.

Download all of the node dependencies:

~~~bash
yarn install
~~~

Run the project locally

~~~bash
npm run watch
~~~

Go to <http://localhost:3000>

As you make changes, the browser will reload 

# Deployment

Stop the local server if it is running by pressing `CTRL-C` in the window where you're running `npm run watch`.

Follow [these instructions](https://docs.mobilelocker.com/v3.0/reference#upload-a-presentation-programatically) to get your API key and put it into the `$HOME/.mobilelocker` file.

Create a file in this project's folder named `site_config`. Put this into it:

~~~bash
PRESENTATION_ID=YOUR_PRESENTATION_D
~~~

Where **YOUR_PRESENTATION_ID** is the ID of your Presentation in the Mobile Locker website.

Run this on the command line. It will package up the project and upload it to Mobile Locker.

~~~bash
./deploy-prod.sh
~~~

# Learning Materials and References

## VueJS

* [Learn Vue 2: Step by Step](https://laracasts.com/series/learn-vue-2-step-by-step) - awesome video series from Laracasts.com
* [VueJS Guide](https://vuejs.org/v2/guide/)
* [VueJS API](https://vuejs.org/v2/api/)
* [Vue Router](https://router.vuejs.org/en/index.html)
* [Vuelidate](https://monterail.github.io/vuelidate/) - Simple, lightweight model-based validation for Vue.js 2.0
* [Made With Vue](https://madewithvuejs.com/ui-components) - A collection of web projects made with vue.js
* [Awsome Vue](https://github.com/vuejs/awesome-vue) - "A curated list of awesome things related to Vue.js"
* [Axios HTTP Library](https://github.com/mzabriskie/axios)
* [Compare Vue to other frameworks](https://vuejs.org/v2/guide/comparison.html)

## CSS and SASS

* [Block Element Modifier Convention](http://getbem.com/introduction/) - "BEM"

